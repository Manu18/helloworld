public class Stack {
    String stackInhalt;
    Object[] stack;
    public int maximalGroesse;

    public Stack(int maximalGroesse) {
        this.maximalGroesse = maximalGroesse;
        this.stack = new Object[maximalGroesse];
    }

    public void push(Object o) {
        for (int i = 0; i < maximalGroesse; i++) {
            if(stack[i] == null) {
                stack[i] = o;
                break;
            }
        }
    }

    public Object pop() {
        String ausgabe = "";
        for (int i = 0; i < maximalGroesse; i++) {
            if(stack[i] == null) {
                ausgabe = String.valueOf(stack[i-1]);
                stack[i-1] = null;
                return ausgabe;
            }
        }
        return ausgabe;
    }

    public boolean isEmpty() {
        boolean empty = true;
        for (int i = 0; i < maximalGroesse; i++) {
            if(stack[i] != null) {
                empty = false;
            }
        }
        return empty;
    }
}
