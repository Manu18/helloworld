import java.util.Arrays;

public class Vertauschen {
    public static String[] vertauscheInhalt(String[] args) {
        String[] erg = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            erg[(args.length - (1 + i))] = args[i];
        }
        return erg;
    }

    public static String[] vertauscheReferenz(String[] args) {
        String cache = " ";
        for (int i = 0; i < (args.length / 2); i++) {
            cache = args[i];
            args[i] = args[(args.length - (1 + i))];
            args[(args.length - (1 + i))] = cache;
        }
        return args;
    }

    public static boolean testInhaltGleich(String[] arg1, String[] arg2) {
        boolean gleich = false;
        for (int i = 0; i < arg1.length; i++) {
            if(!arg1[i].equals(arg2[i])) {
                return false;
            }
            gleich = true;
        }
        return gleich;
    }

    public static boolean testReferenzGleich(String[] arg1, String[] arg2) {
        boolean gleich = arg1.hashCode() == arg2.hashCode();
        return gleich;
    }

    public static void main(String[] args) {
        String[] inhalt = vertauscheInhalt(args);
        String[] referenz = vertauscheReferenz(args);

        System.out.println(Arrays.toString(inhalt));
        System.out.println(Arrays.toString(referenz));

        if (testInhaltGleich(inhalt, referenz)) {
            System.out.println("Fehler: Inhalte stimmen nicht ueberein!");
        } else {
            System.out.println("Inhalt gleich funktioniert");
        }

        if (testReferenzGleich(inhalt, referenz)) {
            System.out.println("Fehler: Referenzen muessen unterschiedlich sein!");
        } else {
            System.out.println("Referenz funktioniert");
        }

    }

}
