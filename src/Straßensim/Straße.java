package Straßensim;

import java.util.Arrays;

public class Straße {
    String[][] fahrbahn;
    //int[] geschwindigkeiten;
    Fahrzeug[] fahrzeuge;
    public static int straßenlänge = 100 + 1;

    public Straße(int autos, int[] position, int[] geschwindigkeit) {
        fahrbahn = new String[straßenlänge][2];
        for (int i = 0; i < straßenlänge; i++) {
            fahrbahn[i][0] = ".";
            fahrbahn[i][1] = "0";
        }
        for (int i = 0; i < autos; i++) {
            fahrbahn[position[i]][0] = "1";
            fahrbahn[position[i]][1] = String.valueOf(geschwindigkeit[i]);
        }

        this.fahrzeuge = new Fahrzeug[autos];
        for (int i = 0; i < autos; i++) {
            fahrzeuge[i] = new Fahrzeug(position[i], geschwindigkeit[i]);
        }

    }

    public void simulation() {
        for (int i = 0; i < fahrzeuge.length; i++) {
            int abstand = (fahrzeuge[(i+1) % fahrzeuge.length].altePosition - fahrzeuge[i].altePosition);
            if (abstand < 0) {
                abstand += straßenlänge;
            }
            if (abstand >= 5) {
                if (fahrzeuge[i].geschwindigkeit < 5) {
                    fahrzeuge[i].beschleunigen();
                }
                fahrzeuge[i].zufallBremsen();
            }
            if (abstand < fahrzeuge[i].alteGeschwindigkeit) {
                fahrzeuge[i].bremsen(abstand);
            }
        }
        for (int i = 0; i < fahrzeuge.length; i++) {
            // alle fahren 1x
            fahrzeuge[i].fahren();
        }

        for (int i = 0; i < straßenlänge; i++) {
            fahrbahn[i][0] = ".";
            fahrbahn[i][1] = "0";
        }

        for (int i = 0; i < fahrzeuge.length; i++) {
            fahrzeuge[i].altePosition = fahrzeuge[i].position;
            fahrzeuge[i].alteGeschwindigkeit = fahrzeuge[i].geschwindigkeit;
            fahrbahn[fahrzeuge[i].position][0] = "1";
            fahrbahn[fahrzeuge[i].position][1] = String.valueOf(fahrzeuge[i].geschwindigkeit);
        }

    }

    public String toString() {
        String ausgabe = "";
        for (int i = 0; i < straßenlänge; i++) {
            if (fahrbahn[i][0].equals("1")) {
                ausgabe += fahrbahn[i][1] + " ";
            } else {
                ausgabe += ". ";
            }
        }
        return ausgabe + "\n";
    }
    //n slots 0 bis n-1 nummeriert - letzter slot wird wieder in den ersten gepackt
    // pro slot ein fahrzeug

    // geschwindigkeit 0 und vmax = 5.
    // fahrzeug position  0 ≤ pos ≤ n−1,
// des Systems wird beschrieben durch den aktuellen Zustand der Fahrbahn (in welchem Slot ein Straßensim.Fahrzeug existiert / nicht existiert) und den
// aktuellen Zustand aller Fahrzeuge (welche Positon, welche Geschwindigkeit zu jedem Straßensim.Fahrzeug).

}