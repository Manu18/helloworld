package Straßensim;

public class Fahrzeug {
    int position;
    int geschwindigkeit;
    int altePosition;
    int alteGeschwindigkeit;

    public Fahrzeug(int position, int geschwindigkeit) {
        this.position = position;
        this.altePosition = position;
        this.geschwindigkeit = geschwindigkeit;
        this.alteGeschwindigkeit = geschwindigkeit;
    }

    public void beschleunigen() {
        if (this.geschwindigkeit < 5) {
            this.geschwindigkeit += 1;
        }
    }

    public void bremsen(int abstand) {
        this.geschwindigkeit = abstand - 1;
    }

    public void zufallBremsen() {
        if ((Math.random() >= 0.9) && (this.geschwindigkeit != 0))  {
            this.geschwindigkeit -= 1;
        }
    }

    public void fahren() {
        this.position = (this.position + this.geschwindigkeit) % (Straße.straßenlänge);
    }

}
