package Straßensim;

public class StaßenSimmulation {
    public static void main(String[] args) {
        // 15 zufallspositionen 0-120 + 15* zufallsgeschwindigkeiten von 0-5
        int autos = 10;
        int simulationen = 10;
        int straßenlänge = 100;
        int[] positionen = new int[autos];
        int[] geschwindigkeiten = new int[autos];
        for (int i = 0; i < autos; i++) {
            positionen[i] = (int) (Math.random() * straßenlänge);
            geschwindigkeiten[i] = (int) (Math.random() * 5);
        }
        Straße test = new Straße(autos, positionen, geschwindigkeiten);
        System.out.println(test);
        for (int i = 0; i < simulationen; i++) {
            test.simulation();
            System.out.println(test);
        }


    }
}
