package Grundgerüste;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Eingabe {
/*
    // Grundgerüste.Eingabe Tastatur
    public class Addierer {
        public static void main(String[] args) {
            Scanner s = new Scanner(System.in);
            int x = s.nextInt();
            int y = s.nextInt();
            int z = s.nextInt();
            int ergebnis = x+y+z;
            System.out.println(ergebnis);
        }
    }

    // Grundgerüste.Eingabe Kommandozeile
    public class Addierer {
        public static void main(String[] args) {
            int x = Integer.parseInt(args[0]);
            int y = Integer.parseInt(args[1]);
            int z = Integer.parseInt(args[2]);
            int ergebnis = x+y+z;
            System.out.println(ergebnis);
        }
    }
*/
    // Grundgerüste.Eingabe Datei
    //public class Addierer {
        public static void main(String[] args) throws IOException {
            FileReader fr = new FileReader("C:\\Users\\mail\\Desktop\\test.txt");
            BufferedReader br = new BufferedReader(fr);

            int x =  Integer.parseInt(br.readLine());
            int y =  Integer.parseInt(br.readLine());
            int z =  Integer.parseInt(br.readLine());

            int ergebnis = x+y+z;
            System.out.println(ergebnis);
        }
    //}
}
