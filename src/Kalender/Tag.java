package Kalender;

public class Tag {
    String termin;
    int prioritaet;

    public Tag() {
        this.termin = null;
        this.prioritaet = 0;
    }

    public void eintragen(String was, int prioritaet) {
        this.termin = was;
        this.prioritaet = prioritaet;
    }

    public String getVerabredung() {
        /*if (this.termin.equals(null)) {
            return "none";
        } else {
        */
        return termin;
        //}

    }

    public int getPrioritaet() {
        return prioritaet;
    }
}
