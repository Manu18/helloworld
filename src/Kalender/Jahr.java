package Kalender;

public class Jahr {
    int jahreszahl;
    public Tag[] tage = new Tag[365];

    public Jahr(int jahreszahl) {
        for (int i = 0; i < tage.length; i++) {
            tage[i] = new Tag();
        }
        this.jahreszahl = jahreszahl;
    }

    public void eintragen(int tag, String was, int prioritaet) {
        if (tage[tag-1].getVerabredung() == null) {
            tage[tag-1].eintragen(was, prioritaet);
        } else {
            System.out.println("Fehler: " + tage[tag-1].getVerabredung());
        }
    }

    public String getTermin(int tag) {
        return tage[tag-1].getVerabredung();
    }

    public int getPrioritaet(int tag) {
        return tage[tag-1].getPrioritaet();
    }

    public int getBelegt() {
        int belegteTage = 0;
        for (int i = 0; i < tage.length; i++) {
            if (tage[i].getVerabredung() != null) {
                belegteTage++;
            }
        }
        return belegteTage;
    }

}
