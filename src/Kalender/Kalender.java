package Kalender;

public class Kalender {
    public static void main(String[] args) {
        Jahr jahr2018 = new Jahr(2018);
        Jahr jahr2019 = new Jahr(2019);

        jahr2018.eintragen(45, "Klausur", 1);
        jahr2019.eintragen(37, "Treffen", 5);

        tagBelegt(jahr2018, 17);
        tagBelegt(jahr2018, 45);

        int belegteTage = jahr2018.getBelegt() + jahr2019.getBelegt();
        System.out.println("Belegte Tage insgesamt: " + belegteTage);
    }

    public static void tagBelegt(Jahr jahr, int tag) {
        if (jahr.getTermin(tag) == null) {
            System.out.println(tag + ". Kalender.Tag ist frei");
        } else {
            System.out.println(tag + ". Kalender.Tag ist belegt");
        }
    }
}
