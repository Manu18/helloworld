public class Fehlermeldung extends RuntimeException {
    public Fehlermeldung() {
        super("Ein Fehler ist aufgetreten!");
    }
    public Fehlermeldung(String message) {
        super(message);
    }
}
