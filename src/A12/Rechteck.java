package A12;

public class Rechteck {
    int x1;
    int x2;
    int y1;
    int y2;
    public Rechteck(int x1, int x2, int y1, int y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
    }
    public Rechteck(Punkt p1, Punkt p2) {
        this.x1 = (int) p1.getX();
        this.x2 = (int) p2.getX();
        this.y1 = (int) p1.getY();
        this.y2 = (int) p2.getY();
    }
    public Punkt eckeLinksOben() {
        return new Punkt(x1,y2);
    }
    public Punkt eckeLinksUnten() {
        return new Punkt(x1,y1);
    }
    public Punkt eckeRechtssOben() {
        return new Punkt(x2,y2);
    }
    public Punkt eckeRechtsUnten() {
        return new Punkt(x2,y1);
    }
    public boolean enhaelt(Punkt p) {
        if ((p.getX() <= x2 && p.getX() >= x1) && (p.getY() <= y2 && p.getY() >= y1)) {
            return true;
        } else {
            return false;
        }
    }
    public Rechteck huelle(Rechteck r) {
        int rx1 = this.x1;
        if (rx1 < r.x1) {
            rx1 = r.x1;
        }
        int rx2 = this.x2;
        if (rx2 > r.x2) {
            rx2 = r.x2;
        }
        int ry1 = this.y1;
        if (ry1 < r.y1) {
            ry1 = r.y1;
        }
        int ry2 = this.y2;
        if (ry2 > r.y2) {
            ry2 = r.y2;
        }
        return new Rechteck(rx1,rx2,ry1,ry2);
    }
    public Rechteck huelle(Punkt p) {
        int px1 = this.x1;
        int px2 = this.x2;
        int py1 = this.y1;
        int py2 = this.y2;
        if (px1 > p.getX()) {
            px1 = (int) p.getX();
        }

        if (px2 < p.getX()) {
            px2 = (int) p.getX();
        }

        if (py1 > p.getY()) {
            py1 = (int) p.getY();
        }

        if (py2 < p.getY()) {
            py2 = (int) p.getY();
        }
        return new Rechteck(px1,px2,py1,py2);
    }
}
