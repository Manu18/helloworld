package A12;

public class Punkt {
    // Instanzvariablen: Koordinaten des Punktes
    private double x;
    private double y;
    // Klassenvariable
    private static int anzahlPunkte;

    // parameterloser Konstruktor
    public Punkt() {
        // rufe anderen Konstruktor mit Default-Werten auf
        this(0.0, 0.0);
    }

    // Konstruktor
    public Punkt(double x, double y) {
        this.x = x;
        this.y = y;
        // ein Punkt mehr
        anzahlPunkte++;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    // verschiebe Punkt
    public void move(Punkt p) {
        x += p.x;
        y += p.y;
    }

    // erzeuge String-Darstellung
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    // Klassenmethode
    public static int getAnzahlPunkte() {
        return anzahlPunkte;
    }
}