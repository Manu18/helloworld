public class Studierender {
    String vorname;
    String nachname;
    long martikelnummer;
    float[] pruefungsergbnisse = new float[500];
    float notendurchschnitt;
    //double[] verfügbareNoten = {1.0,1.3,1.7,2.0,2.3,2.7,3.0,3.3,3.7,4.0,5.0};

    public Studierender(String vorname, String nachname, long martikelnummer) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.martikelnummer = martikelnummer;
    }

    public void neueNote(float note) {
        boolean fertig = false;
        for (int i = 0; (i < pruefungsergbnisse.length) && !fertig; i++) {
            if(pruefungsergbnisse[i] == 0) {
                pruefungsergbnisse[i] +=note;
                fertig = true;
            }
        }
        getNotendurchschnitt();
    }

    public float getNotendurchschnitt() {
        float durchscnitt = 0;
        int zähler = 0;
        for(int i = 0; (i < pruefungsergbnisse.length) && (pruefungsergbnisse[i] != 0); i++) {
            durchscnitt += pruefungsergbnisse[i];
            zähler++;
        }
        notendurchschnitt = (durchscnitt / zähler);
        return notendurchschnitt;
    }

    public String toString() {
        String pergbnisse = "";
        for (int i = 0; i < pruefungsergbnisse.length && pruefungsergbnisse[i] != 0; i++) {
            pergbnisse += pruefungsergbnisse[i] + " ";
        }
        String ausgabe = "Name: " + this.vorname + " " + this.nachname +", Matr.Nr.: " + this.martikelnummer + ", Ergebnisse " + pergbnisse + ", Notendurchschnitt: " + this.notendurchschnitt;
        return ausgabe;
    }


    public static void main(String[] args) {
        Studierender s1 = new Studierender("Willi", "Winzig", 123456);
        Studierender s2 = new Studierender("Helga", "Hurtig", 123457);
        s1.neueNote(3.0F);
        s1.neueNote(2.3F);
        s2.neueNote(1.3F);
        System.out.println(s1);
        System.out.println(s2);

    }
}
