import java.util.*;

public class Fibonacci {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		System.out.println(Arrays.toString(fibanacciReihe(n)));
	
	}
	public static int[] fibanacciReihe(int n) {
		int[] fib = new int[n];
		for(int i = 0; i < n; i++) {
			fib[i] = fibanacci(i);
		}
		return fib;
	}
	
	public static int fibanacci(int k) {
		if (k == 0) {
			return 0;
		}
		if (k == 1) {
			return 1;
		} else {
			return fibanacci(k-1) + fibanacci(k-2);
		}
	}
	

}