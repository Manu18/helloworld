public class StudisTest {
    public static void main(String[] args) {
        //int anzahl = Integer.parseInt(args[0]); // durch 6 teilbar
        int anzahl = 36;
        Studis[] studenten = Studis.neueStudis(anzahl);

        for (int i = 0; i < studenten.length; i++) {
            if(i < (studenten.length / 2)) {
                studenten[i].ersteHaelfte(studenten);
            } else {
                studenten[i].zweiteHaelfte(studenten);
            }
        }

        for (int i = 0; i < anzahl; i++) {
            System.out.println(studenten[i].lernfortschritt);
        }

    }
}
