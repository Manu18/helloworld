public class Ber {
	public static void main(String[] args) {
		double a = Double.parseDouble(args[0]);
		double b = Double.parseDouble(args[1]);
		System.out.println(b(a,b));
	
	}
	public static double b(double x, double k) {
		if (k == 0) {
			return 1.0;
		}
		if (k > 0) {
			return x*b(x,k-1);
		} else {
			return (1d / b(x,-k));
		}
	
	}
}