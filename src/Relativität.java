public class Relativität {
    public static double wachstum(int t, double k) {
        double wachstum;
        double e = Math.E;
        int c = 300000000; //Lichtgeschwindigkeit angenähert
        wachstum = c - (c - 0)*Math.pow(e, (-k*t));

        return wachstum;
    }
    public static void main(String[] args) {
        int körperGewicht = 100; //kg
        double v1 = 0; //Lichtgeschwindigkeit angenähert
        int c = 300000000; //Lichtgeschwindigkeit angenähert
        double dymMasse = 0;
        double wurzelAusdruck = 0;

        for (int i = 1; (i < 1000) && (v1 < c); i++) {
            System.out.println("Masse (kg): " + dymMasse);
            System.out.println("Geschwindigkeit (m/s): " + v1);

            v1 = wachstum(i, 0.0001);
            dymMasse = körperGewicht / (Math.sqrt(1.0 - (v1*v1) / (c*c)));
        }
    }
}
