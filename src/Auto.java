public class Auto {
	private int preis;
	private double tankfuellung;
	private double tankgroesse;
	private double tankzeit;
	private double treibstoffpreis;
	private double verbrauch;
	private int kilometerstand;
	private String antrieb;
	private int gewicht;
	
	public Auto(int preis, int tankfuellung, double tankgroesse, double tankzeit, double treibstoffpreis, String antrieb, double verbrauch, int gewicht) {
		this.preis = preis;
		this.tankfuellung = tankfuellung;
		this.kilometerstand = 0;
		this.tankgroesse = tankgroesse;
		this.tankzeit = tankzeit;
		this.treibstoffpreis = treibstoffpreis;
		this.verbrauch = verbrauch;
		
		//subklassen
		this.antrieb = antrieb;
		this.gewicht = gewicht;
	
	}

	public void fahren(int kilometer) {
		this.kilometerstand += kilometer;
		int strecke = kilometer;
			while (strecke != 0 && tankfuellung >= 0) {
				if(tankfuellung >= 3) {
					this.tanken();
				} else {
					this.tankfuellung -= (int) (verbrauch/100d) * 1;
					strecke--;
				}
			}
			System.out.print(".");
	}
		


	void tanken() {
		int verbraucht = (int) (this.tankgroesse - this.tankfuellung);
		this.tankfuellung = this.tankgroesse;
		for (int i= 0; i < verbrauch; i++) {
			System.out.print("-");
		}
		
	}
	
	
	public String toString() {
		String s = "Eigenschaften: \n" + "Preis: " + this.preis + "\n Kilometerstand: " + this.kilometerstand + "\n Verbrauch: " + this.verbrauch;
		return s;
		
	}


}