import java.util.Arrays;
public class Rueckgabeautomat {
    public static void main(String[] args) {
        double einzahlbetrag = 1.0;
        double zahlbetrag = 8.57;
        int[] verfügbar = {5,5,5,5,5,5,5,5};
        int[] verfügbar2 = {2,2,2,2,2,2,2,2};
        Rueckgabeautomat r = new Rueckgabeautomat(verfügbar);
        //System.out.println(Arrays.toString(r.aktuellerBestand(r)));
        r.einzahlung(einzahlbetrag);
        System.out.println(Arrays.toString(r.aktuellerBestand(r)));
        System.out.println(Arrays.toString(r.auszahlung(zahlbetrag)));

        System.out.println("\n");

        Rueckgabeautomat ra = new Rueckgabeautomat(verfügbar2);
        //System.out.println(Arrays.toString(ra.aktuellerBestand(ra)));
        ra.einzahlung(einzahlbetrag);
        System.out.println(Arrays.toString(ra.aktuellerBestand(ra)));
        System.out.println(Arrays.toString(ra.auszahlung(zahlbetrag)));
    }

    int[] geldstuecke = {200,100,50,20,10,5,2,1}; //Cent
    int[] verfügbareGeldstuecke = new int[8];

    public Rueckgabeautomat(int[] verfügbareGeldstuecke) {
        this.verfügbareGeldstuecke = verfügbareGeldstuecke;
    }

    public int[] aktuellerBestand(Rueckgabeautomat ra) {
        int[] verfügbar = ra.verfügbareGeldstuecke;
        return verfügbar;
    }

    public void einzahlung(double einzahlung) {
        einzahlung = einzahlung * 100;
        for (int i = 0; i < geldstuecke.length; i++) {
            if (geldstuecke[i] == (int) einzahlung) {
                verfügbareGeldstuecke[i]++;
            }
        }
        //System.out.println("Geld eingezahlt: " + einzahlung);
    }

    public int[] auszahlung(double betrag) {
        int[] bennötigteMünzen = Münzen.münzautomat(betrag);
        for (int i = 0; i < verfügbareGeldstuecke.length; i++) {
            if (verfügbareGeldstuecke[i] < bennötigteMünzen[i]) {
                ///////if münzautomat2 hat nicht genug { }
                System.out.println("Fehler: Nicht genug Rückgeld vorhanden!");
                return bennötigteMünzen;
            }
        }
        return bennötigteMünzen;
    }
}
