import java.util.*;

public class TestMatrix {
	public static void main(String[] args) {
		int[][] a = matrix(10);
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}

	}
	
	public static int[][] matrix2(int n) {
		int[][] a = new int[n][n];	
		int zähler = 0;
		for (int i = zähler; i < n; i++) {
			for(int j = 0; j < n - zähler; j++) {
				a[i][j] = n - zähler;	
			}
			zähler++;
		}
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if(a[i][j] == 0) {
					a[i][j] = j + 1;
				}
			}
		}
		return a;
	}

	public static int[][] matrix(int n) {
		int[][] a = new int[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n-i; j++) {
				a[i][j]= n - i;
			}
		}
		for (int i = 0; i < n; i++) {
			for (int j = (n-1-i); j < n; j++) {
				a[j][i] = i + 1;
			}
		}
		return a;
	}
}