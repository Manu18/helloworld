public class MS_Datum {
    public static int berechneJahr(int days) {
        int year = 1980; // 01.01.1980 ist der Kalender.Tag 1
        while (((days > 365) && !isLeapYear(year)) || (isLeapYear(year) && (days > 366))) {
            // WIeso 365, müsste eine falluntersscheidung geben, ob schalltjahr oder nicht.
            if (isLeapYear(year)) {
                if (days > 366) {
                    days = days - 366;
                    year = year + 1;
                }
            } else {
                days = days - 365;
                year = year + 1;
            }
        }
        return year;
    }

    public static boolean isLeapYear(int jahr) {
        boolean istSchaltjahr = false;
        istSchaltjahr = ( (   (jahr % 4) == 0) && (!((jahr % 100) == 0))) || ((jahr % 400) == 0);
        return istSchaltjahr;
    }

    public static void main(String[] args) {

        int jahr = berechneJahr(10593);
        //int jahr = berechneJahr(10583);
        System.out.println(jahr);

    }


}
