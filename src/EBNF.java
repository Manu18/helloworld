import java.util.Scanner;

public class EBNF {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String eingabe = sc.nextLine();
        char c ='A';
        String eingabeVor = "";
        String eingabeNach = "";
        String ausgabeVor = "";
        String ausgabeNach = "";
        if (eingabe.startsWith("vor")) {
            eingabeVor = eingabe.substring(9);
            c = eingabe.charAt(4);
            String s = Character.toString(c);
            if (eingabeVor.contains(s)) {
                for (int i = 0; (eingabeVor.charAt(i) != c); i++) {
                    ausgabeVor = eingabeVor.substring(0, i + 1);
                }
            } else {
                ausgabeVor = "Nicht vorhanden";
            }
        }
        if (eingabe.startsWith("nach")) {
            c = eingabe.charAt(5);
            eingabeNach = eingabe.substring(10);
            String s = Character.toString(c);
            if (eingabeNach.contains(s)) {
                ausgabeNach = eingabeNach.substring(1);
                for (int i=0; (eingabeNach.charAt(i) != c); i++) {
                    ausgabeNach = eingabeNach.substring(i+2);
                }
            } else {
                ausgabeNach = "Nicht vorhanden";
            }
        }
        System.out.println(eingabeVor);
        System.out.println(ausgabeVor);
        System.out.println(eingabeNach);
        System.out.println(ausgabeNach);
    }
}

