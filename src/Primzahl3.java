public class Primzahl3 {
    public static void main(String[] args) {
        //int xmax = Integer.parseInt((args[0]));
        int xmax = 7;
        int[] p = erzeugen(xmax);
        System.out.println(ueberpruefen(p, xmax));

    }

    public static int[] erzeugen(int n) {
        int[] prim = new int[n-1];
        for(int i = 2; i <= n; i++) {
            prim[i-2] = i;
        }
        for (int i = 0; i < prim.length; i++) {
            if(prim[i] != 0) {
                for (int k = i+1; k < prim.length; k++) {
                    if(prim[k] % prim[i] == 0) {
                        prim[k] = 0;
                    }
                }
            }
        }
        int[] erg = new int[0];
        for (int i = 0; i < prim.length; i++) {
            if(prim[i] != 0) {
                int[] k = new int[erg.length + 1];
                for (int j = 0; j < erg.length; j++) {
                    k[j] = erg[j];
                }
                k[erg.length] = prim[i];
                erg = k;
            }
        }
        return erg;
    }
    public static boolean ueberpruefen(int[] p, int n) {
        for (int i = 0; i < p.length; i++) {
            int f = 1;
            for (int j = 1; j <= p[i] - 1; j++) {
                f *= j;
            }
            f++;
            if (f % p[i] != 0) {
                return false;
            }
        }
        return true;
    }
}
