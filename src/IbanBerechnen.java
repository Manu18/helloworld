import java.util.Scanner;

public class IbanBerechnen  {
    public static String kontonrVerlaengern(String kn) {
        String kns = kn;
        while (kns.length() < 10) {
            kns = "0" + kns;
        }
        return kns;
    }
    public static String buchstabeZahl(String laendercode, char[] alphabet) {
        int laenderzahl1 = 0;
        int laenderzahl2 = 0;
        String laenderZusatz;
        for (int i=0; alphabet[i] != laendercode.charAt(0) && (i < 26); i++) {
            laenderzahl1 = 9 + i + 2;
        }
        for (int i=0; ((alphabet[i] != laendercode.charAt(1)) && (i < 26)); i++) {
            laenderzahl2 = 9 + i + 2;
        }
        laenderZusatz = laenderzahl1 + "" + laenderzahl2 + "00";
        return laenderZusatz;
    }

    public static String prüfsumme(String zusammengefuegt) {
        String prüfsumme = "";
        String gekürzteIban;
        long modulo = 0;
        int a = 9;
        int c = 9;
        int letztePos = 0;
        boolean brichab = false;

        gekürzteIban = zusammengefuegt.substring(letztePos, a);
        letztePos = 9;
        do {
            modulo = Long.parseLong(gekürzteIban) % 97;
            a = a + (9 - String.valueOf(modulo).length());
            if ((String.valueOf(modulo).length() + zusammengefuegt.substring(a).length()) <= 9) {
                int b = 0;
                for (int i = letztePos; (i < (letztePos + 9)) && (i < zusammengefuegt.length()); i++) {
                    b = i;
                }
                gekürzteIban = modulo + zusammengefuegt.substring(letztePos, b);
                modulo = Long.parseLong(gekürzteIban) % 97;
                letztePos = b;
                if(letztePos < zusammengefuegt.length()) {
                    gekürzteIban = modulo + zusammengefuegt.substring(letztePos);
                    modulo = Long.parseLong(gekürzteIban) % 97;
                }
                brichab = true;
            } else {
                gekürzteIban = modulo + zusammengefuegt.substring(letztePos, a);
                letztePos = a;
            }
        } while(!brichab);
        modulo = 98 - modulo;
        prüfsumme = String.valueOf(modulo);
        if (prüfsumme.length() == 1) {
            prüfsumme = "0" + prüfsumme;
        }
        return prüfsumme;
    }

    public static String erzeugeIban(String lc, String blz, String kn) {
        char[] alphabet = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        String laenderkennungGross = lc.toUpperCase();
        String kontonummer = kontonrVerlaengern(kn);
        String bankleitzahl = blz;
        String bban = bankleitzahl + kontonummer;
        String laenderZusatz = buchstabeZahl(laenderkennungGross, alphabet);
        String zusammengefuegt = bban + laenderZusatz;
        String pruefZahl = prüfsumme(zusammengefuegt);
        String iban = laenderkennungGross + pruefZahl + bban;
        return iban;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String lc = sc.next();
        String blz = sc.next();
        String kn = sc.next();
        sc.close();

        String iban = erzeugeIban(lc, blz, kn);
        System.out.println(iban);
    }
}