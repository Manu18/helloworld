public class Klausurnoten {
    public static void verarbeiteKlausurPunktzahl(int[] punkte, int[] punktGrenzen) {
        //Vereinfachung gibt es nur die ganzen Noten 1,2,3,4,5.
        int teilnehmer = punkte.length;
        int besteNote = 0;
        int bestePunkte = 0;
        for (int i = 0; i < teilnehmer; i++) {
            if (bestePunkte < punkte[i]) {
                bestePunkte = punkte[i];
            }
        }
        if (bestePunkte <= punktGrenzen[0]) {
            besteNote = 5;
        }
        if ((bestePunkte <= punktGrenzen[1]) && (bestePunkte > punktGrenzen[0])) {
            besteNote = 4;
        }
        if ((bestePunkte <= punktGrenzen[2]) && (bestePunkte > punktGrenzen[1])) {
            besteNote = 3;
        }
        if ((bestePunkte <= punktGrenzen[3]) && (bestePunkte > punktGrenzen[2])) {
            besteNote = 2;
        }
        if (bestePunkte > punktGrenzen[3]) {
            besteNote = 1;
        }
        int schlechtestePunkte = 1000000000;
        int schlechtesteNote = 5;
        for (int i = 0; i < teilnehmer; i++) {
            if (schlechtestePunkte >= punkte[i]) {
                schlechtestePunkte = punkte[i];
            }
        }
        if (schlechtestePunkte <= punktGrenzen[0]) {
            schlechtesteNote = 5;
        }
        if ((schlechtestePunkte <= punktGrenzen[1]) && (schlechtestePunkte > punktGrenzen[0])) {
            schlechtesteNote = 4;
        }
        if ((schlechtestePunkte <= punktGrenzen[2]) && (schlechtestePunkte > punktGrenzen[1])) {
            schlechtesteNote = 3;
        }
        if ((schlechtestePunkte <= punktGrenzen[3]) && (schlechtestePunkte > punktGrenzen[2])) {
            schlechtesteNote = 2;
        }
        if (schlechtestePunkte > punktGrenzen[3]) {
            schlechtesteNote = 1;
        }

        int bestanden = 0;
        int nichtBestanden = 0;
        for (int i = 0; i < teilnehmer; i++) {
            if (punkte[i] > punktGrenzen[0]) {
                bestanden++;
            }
        }
        nichtBestanden = teilnehmer - bestanden;
        double durchschnitt = 0;
        for (int i = 0; i < teilnehmer; i++) {
            durchschnitt += punkte[i];
        }
        durchschnitt /= teilnehmer;

        int note1 = 0;
        int note2 = 0;
        int note3 = 0;
        int note4 = 0;
        int note5 = 0;
        // Note // Anzahl
        for (int i = 0; i < teilnehmer; i++) {
            if (punkte[i] <= punktGrenzen[0]) {
                note5++;
            }
            if ((punkte[i] <= punktGrenzen[1]) && (punkte[i] > punktGrenzen[0])) {
                note4++;
            }
            if ((punkte[i] <= punktGrenzen[2]) && (punkte[i] > punktGrenzen[1])) {
                note3++;
            }
            if ((punkte[i] <= punktGrenzen[3]) && (punkte[i] > punktGrenzen[2])) {
                note2++;
            }
            if (punkte[i] > punktGrenzen[3]) {
                note1++;
            }
        }

        System.out.println(teilnehmer);
        System.out.println(besteNote + " " + schlechtesteNote);
        System.out.println(bestanden + " " + nichtBestanden);
        System.out.println(durchschnitt);
        System.out.println("1 " + note1);
        System.out.println("2 " + note2);
        System.out.println("3 " + note3);
        System.out.println("4 " + note4);
        System.out.println("5 " + note5);

    }
    public static void main(String[] args) {
        int[] punkte = {1, 1, 2, 3, 4, 4, 5, 10};
        int[] punktGrenzen = {0, 1, 2, 3};

        verarbeiteKlausurPunktzahl(punkte, punktGrenzen);
    }

}
