public class Musikstueck {
    String [] interpreten;
    String titel;
    int[] toene;
    int anzahlEinzelBewertungen;
    int gesamtBewertung;


    public Musikstueck(String[] interpreten, String titel, int[] toene) {
        this.interpreten = interpreten;
        this.titel = titel;
        this.toene = toene;
    }

    public void abspielen() {
        String ausgabe = "";
        for (int i = 0; i < toene.length; i++) {
            if (i != (toene.length - 1)) {
                ausgabe += toene[i] + " ";
            } else {
                ausgabe += toene[i];
            }
        }
        System.out.println(ausgabe);
    }

    public void neueBewertung(int bewertung) {
        int bewertungszähler = 0;
        if (getGesamtbewertung() != 0) {
            bewertungszähler = anzahlEinzelBewertungen * gesamtBewertung;
        }
        anzahlEinzelBewertungen++;
        double rechnung = ((double) bewertungszähler + (double) bewertung) / (double) anzahlEinzelBewertungen;
        gesamtBewertung = (int) Math.round(rechnung);
    }

    public int getGesamtbewertung() {
        return this.gesamtBewertung;
    }

    public String toString() {
        String ausgabe = "";
        String interpretenS = "";
        for (int i = 0; i < interpreten.length; i++) {
            interpretenS += interpreten[i] + ", ";
        }
        String toeneS = "";
        for (int i = 0; i < toene.length; i++) {
            toeneS += toene[i] +  " ";
        }
        ausgabe = "Interpreten: " + interpretenS + "Titel: " + this.titel + ", Toene: " + toeneS + ", Anz. Bewertungen: " + this.anzahlEinzelBewertungen + ", Gesamtbewertung: " + getGesamtbewertung();
        return ausgabe;
    }

    public static void main(String[] args) {
        String[] interpreten1 = {"Wham"};
        String[] interpreten2 = {"Koelner Domspatzen", "Heino"};
        int[] toene1 = {1,2,3,4,5,6};
        int[] toene2 = {6,5,4,3,2,1};
        Musikstueck m1 = new Musikstueck(interpreten1, "Last Christmas", toene1);
        Musikstueck m2 = new Musikstueck(interpreten2, "O Tannenbaum", toene2);
        m1.abspielen();
        m1.neueBewertung(1);
        m1.neueBewertung(1);
        m1.neueBewertung(3);
        System.out.println(m1);
        System.out.println(m2);
    }
}
