public class Ratten {
    public static void main(String[] args) {
        int n = 2;
        int nachwuchs = 6;
        int babies = 0;
        int kinder = 0;
        int erwachsene = 1;
        int ratten = 0;

        for (int i = 1; i <= n; i++) {
            erwachsene += kinder;
            kinder = babies;
            babies = erwachsene * nachwuchs;
        }
        ratten = (erwachsene + kinder + babies) * 2;
        System.out.println(ratten);
    }
}
