public class WebShop {
    public static void main(String[] args) {

    }
    Kunde[] k = new Kunde[10];
    Artikel[] a = new Artikel[10];

    public WebShop() {

    }

    Kunde neuerKunde(String vorname, String nachname) {
        int i = 0;
        while (!(k[i] == null)) {
            i++;
        }
        k[i] = new Kunde(vorname, nachname);
        return k[i];
    }

    Kunde neuerKunde(String vorname, String nachname, double nachlass) {
        int i = 0;
        while (!(k[i] == null)) {
            i++;
        }
        k[i] = new GuterKunde(vorname, nachname, nachlass);
        return k[i];
    }
    void neuerArtikel(String name, double preis, int anzahl) {
        int i = 0;
        while (!(a[i] == null)) {
            i++;
        }
        a[i] = new Artikel(name, preis, anzahl);
    }

    String bestellen(Kunde kunde, String[] artikel) {
        String z1 = "";
        double guterKundePreis = 0;
        if (kunde instanceof GuterKunde) {
            z1 = "Rechnung fuer unseren guten Kunden " + kunde.vorname + " " + kunde.nachname + ", Preisnachlass " + (((GuterKunde) kunde).nachlass * 100) + "%: \n";
        } else {
            z1 = "Rechnung fuer " + kunde.vorname + " " + kunde.nachname + ": \n";
        }
        String bestellung = "";
        boolean gefunden = false;
        boolean verfügbar = false;
        int artikelNr = 0;
        double gesamtPreis = 0.0;

        for (int i = 0; i < artikel.length; i++) {
            int j = 0;
            while (j < a.length && a[j] != null) {
                if (a[j].name.equals(artikel[i])) {
                    artikelNr = j;
                    if ((a[artikelNr].bestand) >= 1) {
                        verfügbar = true;
                    }
                    a[artikelNr].bestand -= 1;
                    gefunden = true;

                }
                j++;
            }
            if (gefunden && verfügbar) {
                if (kunde instanceof GuterKunde) {
                    guterKundePreis = a[artikelNr].preis - (a[artikelNr].preis * ((GuterKunde) kunde).nachlass);
                    bestellung += a[artikelNr].name + " : " + guterKundePreis + " \n";
                    gesamtPreis += guterKundePreis;
                } else {
                    bestellung += a[artikelNr].name + " : " + a[artikelNr].preis + " \n";
                    gesamtPreis += a[artikelNr].preis;
                }
            } else if (gefunden && !verfügbar) {
                bestellung += a[artikelNr].name + " : " + "nicht mehr vorhanden \n";
            } else if (!gefunden) {
                bestellung += artikel[i] + " : " + "nicht gefunden \n";
            }
            gefunden = false;
            verfügbar = false;
        }

        String gesamtPreisS = "Gesamtpreis : " + gesamtPreis + " \n";
        return z1 + bestellung + gesamtPreisS;
    }

}
