public class MessReihe {
    int maxAnzahlDatensaetze;
    MessDatum[] messungen;
    MessBereich umdrehung = new MessBereich(0,12000,8000);
    MessBereich temperatur = new MessBereich(0,200,150);
    MessBereich ladedruck = new MessBereich(0,6,5);

    public MessReihe(int maxAnzahlDatensaetze) {
        this.maxAnzahlDatensaetze = maxAnzahlDatensaetze;
        messungen = new MessDatum[maxAnzahlDatensaetze];
    }

    public void neueMessung(int u, double t, double d) throws CriticalValue, IllegalValue, TooMuchData {
        TooMuchData tooMuchData = new TooMuchData();
        umdrehung.pruefen(u);
        temperatur.pruefen(t);
        ladedruck.pruefen(d);
        for (int i = 0; i < maxAnzahlDatensaetze; i++) {
            if(messungen[i] == null) {
                messungen[i] = new MessDatum(u,t,d);
                break;
            }
            if(i+1 == maxAnzahlDatensaetze) {
                throw tooMuchData;
            }
        }
    }

    public double[] ermittleMittelwerte() {
        double mittelwerte[] = new double[3];
        int counter = 0;
        int umdrehungenSum = 0;
        double temperaturSum = 0d;
        double ladedruckSum = 0d;
        for (int i = 0; (i < maxAnzahlDatensaetze) && (messungen[i] != null); i++) {
            umdrehungenSum += messungen[i].u;
            temperaturSum += messungen[i].t;
            ladedruckSum += messungen[i].d;
            counter++;
        }
        mittelwerte[0] = (double) umdrehungenSum / counter;
        mittelwerte[1] = temperaturSum / counter;
        mittelwerte[2] = ladedruckSum / counter;
        return mittelwerte;
    }
}
