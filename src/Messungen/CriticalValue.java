public class CriticalValue extends Exception {
    public CriticalValue() {
        super("Kritischer Wert");
    }
    public CriticalValue(String message) {
        super(message);
    }
}
