public class TooMuchData extends Exception {
    public TooMuchData() {
        super("Zu viele Daten");
    }
    public TooMuchData(String message) {
        super(message);
    }
}
