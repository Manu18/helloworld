public class MessBereich {
    double start;
    double ende;
    double kritisch;

    MessBereich(double start, double ende, double kritisch) {
        this.start = start;
        this.ende = ende;
        this.kritisch = kritisch;
    }

    void pruefen(double wert) throws CriticalValue, IllegalValue {
        CriticalValue critical = new CriticalValue();
        IllegalValue illegal = new IllegalValue();
        if ((wert < start) || (wert > ende)) {
            throw illegal;
        }
        if ((wert >= kritisch) && (wert <= ende)) {
            throw critical;
        }
    }
}
