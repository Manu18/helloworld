public class IllegalValue extends RuntimeException {
    public IllegalValue() {
        super("Ungültiger Wert");
    }
    public IllegalValue(String message) {
        super(message);
    }
}
