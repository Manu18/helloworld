import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class LaborStand {
    public static void main(String[] args) {
        try {
        int maxAnzahlDatensaetze = Integer.parseInt(args[0]);
            MessReihe messReihe;
            try (Scanner sc = new Scanner(new File(args[1]))) {
                messReihe = new MessReihe(maxAnzahlDatensaetze);

                while (sc.hasNext()) {
                    try {
                        int u = sc.nextInt();
                        double t = sc.nextDouble();
                        double d = sc.nextDouble();
                        try {
                            messReihe.neueMessung(u, t, d);
                        } catch (CriticalValue criticalValue) {
                            System.out.println("Fehler: CriticalValue");
                            //criticalValue.printStackTrace();
                            System.exit(1);
                        } catch (IllegalValue illegalValue) {
                            System.out.println("Fehler: IllegalValue");
                            //illegalValue.printStackTrace();
                        } catch (TooMuchData tooMuchData) {
                            System.out.println("Fehler: TooMuchData");
                            //tooMuchData.printStackTrace();
                        }
                    } catch (InputMismatchException e) {
                        System.out.println("Fehler: Falsche Grundgerüste.Eingabe");
                    }
                }
                sc.close();
                double[] mittelwerte = messReihe.ermittleMittelwerte();
                System.out.println("Durchschnitt von Umdrehungszahl: " + mittelwerte[0]);
                System.out.println("Durchschnitt von Zahlen_Funktionen.Temperatur: " + mittelwerte[1]);
                System.out.println("Durchschnitt von Ladedruck: " + mittelwerte[2]);
            } catch (FileNotFoundException e) {
                System.out.println("Fehler: Datei nicht gefunden");
            }
        } catch (NumberFormatException e) {
            System.out.println("Fehler: Falsche Grundgerüste.Eingabe");
        }
    }
}
