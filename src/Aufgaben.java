import java.lang.reflect.Array;
import java.util.Arrays;

public class Aufgaben {
    // A1
    public static void main(String[] args) {
        //for (int i = 0; i < args.length; i++) {
          //  System.out.println(args[i]);
        //}

        int i = 1;
        int k = i++;
        boolean b = (i==k++);
        double x= (double) (++i)/k;
        double c = 0;

        /*
        int[] a = {1,2,3,4};
        int[] b = {1,2,3,4};
        double[] c = {1.0, 14.3};
        double[] d = {1.0, 14.3};
        double[][] e = {{1.1, 14.3}, {1.5, 2.9}};
        double[][] f = {{1.0, 14.3}, {1.5, 2.9}};
        int[][] g = {{1, 2, 3}, {1, 2, 3}};
        int k = 2;
        int[] p = {0,3,4,1,2};

        System.out.println(vergleich(a, k));
        System.out.println(Arrays.toString(umdrehen(a)));
        System.out.println(Arrays.toString(drehen(a))); //////////////////
        System.out.println(Arrays.toString(feldSumme(a, b))); //////////////
        System.out.println(gleich(c, d));
        System.out.println(gleich(e, f));
        double [][] t =  {{1,2,3},{4,5,6},{7,8,9}};
        System.out.println("vorher: ");
        zeigeMatrix(t);
        transponieren(t);
        System.out.println("nachher: ");
        zeigeMatrix(t);
        System.out.println(Arrays.toString(inversePermutation(p)));
        System.out.println(zahlenName(55));

         */
    }

    // A2
    public static int vergleich(int[] feld, int zahl) {
        int gleich = 0;
        for (int k: feld) {
            if(feld[k-1] == zahl) {
                gleich++;
            }
        }
        return gleich;
    }

    public static int[] umdrehen(int[] feld) {
        int[] gedreht = new int[feld.length];
        int k = feld.length -1;
        for (int i = 0; i <= k; i++) {
            gedreht[i] = feld[k-i];
        }
        return gedreht;
    }

    public static int[] drehen(int[] feld) {

        int k = feld.length -1;
        int cache = 0;

        for (int i = 0; i <= (k / 2); i++) {
            cache = feld[i];
            feld[i] = feld[k-i];
            feld[k-i] = cache;
        }
        return feld;
    }

    // A3
    public static int[] feldSumme(int[] feld1, int[] feld2) {
        int[] summe = new int[feld1.length + feld2.length];
        for (int k: feld1) {
            summe[k-1] = feld1[k-1];
        }
        int f = feld1.length;
        for (int k: feld2) {
            summe[f] = feld2[k-1];
            f++;
        }
        return summe;
    }

    // A4
    public static boolean gleich(double[] feld1, double[] feld2) {
        boolean gleich = false;
        double e = 0;
        double summe1 = 0;
        for (int i = 0; i < feld1.length; i++) {
            summe1 += feld1[i];
        }
        double summe2 = 0;
        for (int i = 0; i < feld2.length; i++) {
            summe2 += feld2[i];
        }
        double betrag = Math.abs(summe2 - summe1);
        if(betrag == e) {
            gleich = true;
        }
        return gleich;
    }
    public static boolean gleich(double[][] feld1, double[][] feld2) {
        boolean gleich = false;
        double e = 0;
        double summe1 = 0;
        double summe2 = 0;
        for (int r = 0; r < feld1.length; r++) {
            for (int i = 0; i < feld1[r].length; i++) {
                summe1 += feld1[r][i];
            }
            for (int i = 0; i < feld2[r].length; i++) {
                summe2 += feld2[r][i];
            }
        }
        double betrag = Math.abs(summe2 - summe1);
        if(betrag == e) {
            gleich = true;
        }
        return gleich;
    }

    // A5
    public static void transponieren(double[][] a) {
        for(int i =0; i <a.length ; i ++) {
            // eine Haelfte mit anderer Haelfte tauschen
            for(int j = 0; j < i; j++) {
                // Dreieckstausch mit Spiegelelement der anderen Haelfte
                // (Mittel) Diagonale ist dabei Symmetrieachse
                double tmp = a [i][j];
                a[i][j] = a[j][i];
                a[j][i] = tmp;
            }
        }
    }
    public static void zeigeMatrix(double[][] a) {
        for(int i = 0; i < a.length; i++) {
            for(int j = 0; j < a[i].length; j++) {
                System.out.print(" " + a[i][j]);
            }
            System.out.println();
        }
    }

    //A6
    public static int[] inversePermutation(int[] a) {
        int[] b = new int[a.length];
        // berechne inverse Permutation
        for(int i = 0; i < a.length; i++) {
            b[a[i]] = i;
        }
        return b;
    }

    // A7
    public static String zahlenName(int zahl) {
        String[] zahlen = {"", "", "zwanzig", "dreisig", "vierzig", "fünfzig", "sechzig", "siebzig", "achzig"};
        String[] kleinzahlen = {"", "eins", "zwei", "drei", "vier", "fünf", "sechs", "sieben", "acht", "neun"};
        int zahlV = 0;
        int zahlH = zahl;
        String ergebnis = "";

        if(zahl > 10) {
            zahlH = zahl % 10;
            zahlV = (zahl / 10);
            ergebnis = kleinzahlen[zahlH] + "und" + zahlen[zahlV];
        } else {
            ergebnis = kleinzahlen[zahlH];
        }
        return ergebnis;
    }

    // A9.4
    public static void exchangeAll(int[] f, int a, int b) {
        for (int i = 0; i < f.length; i++) {
            if (f[i] == a) {
                f[i] = b;
            }
        }
        System.out.println(Arrays.toString(f));
    }



}
