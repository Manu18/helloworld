import java.util.Scanner;

public class LaengsteKette {
    public static void main(String[] args) {
        int counter = 1;
        int longest = 1;
        int prevNum = -11116786;

        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int i = sc.nextInt();
            if (i == prevNum) {
                counter++;
            } else {
                if (counter > longest) {
                    longest = counter;
                }
                counter = 1;
            }
            prevNum = i;
        }

        if (counter > longest) {
            longest = counter;
        }

        System.out.println(longest);

    }
}
