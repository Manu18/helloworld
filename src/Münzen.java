import java.util.Scanner;

public class Münzen {
    public static int münzen(int betrag, int[] verfügbareMünzen) { //angabe in cent
        int anzahlMünzen = 0;
        int teilbar = 0;
        for (int i = 0; i < verfügbareMünzen.length; i++) {
            while ((betrag / verfügbareMünzen[i]) >= 1) {
                teilbar = betrag / verfügbareMünzen[i];
                anzahlMünzen += betrag / verfügbareMünzen[i];
                betrag -= teilbar*verfügbareMünzen[i];
            }
        }
        return anzahlMünzen;
    }
    public static int[] benötigteMünzen(int betrag, int[] verfügbareMünzen) {
        int anzahlMünzen = 0;
        int teilbar = 0;
        int[] benöitigteMünzen = new int[verfügbareMünzen.length];

        for (int i = 0; i < verfügbareMünzen.length; i++) {
            while ((betrag / verfügbareMünzen[i]) >= 1) {
                teilbar = betrag / verfügbareMünzen[i];
                benöitigteMünzen[i] = teilbar;
                anzahlMünzen += betrag / verfügbareMünzen[i];
                betrag -= teilbar*verfügbareMünzen[i];
            }
        }
        return benöitigteMünzen;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        float eingabe = s.nextFloat();
        s.close();
        int betrag = (int) (eingabe * 100);
        int[] verfügbareMünzen = {50000, 20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1};
        int münzen = münzen(betrag, verfügbareMünzen);
        int[] benötigteMünzen = benötigteMünzen(betrag, verfügbareMünzen);

        System.out.println("Für den Betrag von " + eingabe + "€ werden " + münzen + " Scheine/Münzen benötigt.");
        System.out.println("Folgende Münzen werden benötigt:");
        for (int i = 0; i < verfügbareMünzen.length; i++) {
            if(benötigteMünzen[i] != 0) {
                if (verfügbareMünzen[i] >= 500) {
                    System.out.println(verfügbareMünzen[i]/100 + " € Scheine: " + benötigteMünzen[i]);
                } else if (verfügbareMünzen[i] >= 100 && (verfügbareMünzen[i] < 500)) {
                    System.out.println(verfügbareMünzen[i]/100 + " € Münzen: " + benötigteMünzen[i]);
                } else {
                    System.out.println(verfügbareMünzen[i] + " Cent Münzen: " + benötigteMünzen[i]);
                }
            }
        }
    }

    public static int[] münzautomat(double eingabe) {
        int betrag = (int) (eingabe * 100);
        int[] verfügbareMünzen = {200, 100, 50, 20, 10, 5, 2, 1};
        int münzen = münzen(betrag, verfügbareMünzen);
        int[] benötigteMünzen = benötigteMünzen(betrag, verfügbareMünzen);

        System.out.println("Für den Betrag von " + eingabe + "€ werden " + münzen + " Scheine/Münzen benötigt.");
        //System.out.println("Folgende Münzen werden benötigt:");
        /*
        for (int i = 0; i < verfügbareMünzen.length; i++) {
            if(benötigteMünzen[i] != 0) {
                if (verfügbareMünzen[i] >= 500) {
                    System.out.println(verfügbareMünzen[i]/100 + " € Scheine: " + benötigteMünzen[i]);
                } else if (verfügbareMünzen[i] >= 100 && (verfügbareMünzen[i] < 500)) {
                    System.out.println(verfügbareMünzen[i]/100 + " € Münzen: " + benötigteMünzen[i]);
                } else {
                    System.out.println(verfügbareMünzen[i] + " Cent Münzen: " + benötigteMünzen[i]);
                }
            }
        }
         */
        return benötigteMünzen;
    }

}
