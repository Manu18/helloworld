package Zahl12;

public class ZahlKlasse {
    private static int anzahl;
    private Zahl zahl;
    public ZahlKlasse(int zahl) {
        ++anzahl;
        this.zahl = new Zahl(zahl);
    }
    private static int getAnzahl() {
        return anzahl;
    }
    public int getZahlWert() {
        return zahl.getZahl();
    }
    public String toString() {
        return "" + zahl.getZahl();
    }

    public class Zahl {
        private int zahl;
        Zahl(int zahl) {
            this.zahl = zahl * ZahlKlasse.getAnzahl();
        }
        int getZahl() {
            return zahl;
        }
    }
}
