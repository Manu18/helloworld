package Zahl12;

class DingMitFarbe {
    protected String farbe;
    public DingMitFarbe(String farbe) {
        this.farbe = farbe;
    }
}
interface FarbeLesen {
    public String getFarbe();
}
