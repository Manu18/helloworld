package Zahl12;

public class Fahrzeug extends DingMitFarbe implements FarbeLesen {
    public Fahrzeug(String farbe) {
        super(farbe);
    }
    @Override
    public String getFarbe() {
        return farbe;
    }
}
