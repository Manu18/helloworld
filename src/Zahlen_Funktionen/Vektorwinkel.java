package Zahlen_Funktionen;

public class Vektorwinkel {
    public static double winkel(double[] a, double[] b) {
        double winkel = 0;
        double betragA = 0;
        double betragB = 0;
        double summeAB = 0;

        for (int i = 0; i < a.length; i++) {
            betragA += Math.pow(a[i], 2);
            betragB += Math.pow(b[i], 2);
            summeAB += (a[i] * b[i]);
        }

        betragA = Math.sqrt(betragA);
        betragB = Math.sqrt(betragB);
        winkel = (summeAB / (betragA * betragB));
        winkel = Math.acos(winkel);
        winkel = winkel * (180/Math.PI);

        return winkel;
    }

    public static void main(String[] args) {

    }
}
