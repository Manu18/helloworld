package Zahlen_Funktionen;
import java.util.Arrays;

public class Primzahl {
        public static void main(String[] args) {
            //int max = Integer.parseInt(args[0]);
            int max = 100;
            int[] primzahlen = erzeugen(max);
            int primzahl = 7;
            System.out.println(Arrays.toString(primzahlen));
            boolean richtig = ueberpruefen(primzahlen, primzahl);
            System.out.println(richtig);

        }

        public static int[] erzeugen(int max) {
            int[] zahlen= new int[max - 1];
            int entfernt = 0;
            for (int i = 0; i <= max -2; i++) {
                zahlen[i] = i + 2;
            }

            for(int i = 0; i < zahlen.length; i++) {
                if (zahlen[i] != 0) {
                    for(int k = zahlen[i] - 1; k < zahlen.length; k++) {
                        if (zahlen[k] != 0) {
                            if(zahlen[k] % zahlen[i] == 0) {
                                zahlen[k] = 0;
                                entfernt++;
                            }
                        }
                    }
                }
            }

            int[] erg = new int[zahlen.length - entfernt];
            int k = 0;
            for(int i = 0; i < zahlen.length; i++) {
                if (zahlen[i] != 0) {
                    erg[k] = zahlen[i];
                    k++;
                }
            }
            return erg;
        }


        public static boolean ueberpruefen(int[] prim, int zahl) {
            boolean richtig = false;
            for (int i = 0; i < prim.length; i++) {
                if(prim[i] == zahl) {
                    int fak =  1;
                    for(int j = zahl - 1; j > 0; j--) {
                        fak *= j;
                    }
                    fak++;
                    richtig = fak % 7 == 0;
                }
            }
            return richtig;
        }
}

