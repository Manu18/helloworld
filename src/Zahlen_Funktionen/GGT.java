package Zahlen_Funktionen;

public class GGT {
    public static void ggT1(int x, int y) {
        if(x == 0) {
            System.out.println(y);
        } else {
            while (y != 0)  {
                if (x > y) {
                    x = x -y;
                } else {
                    y = y -x;
                }
            }
            System.out.println(x);
        }
    }
    public static int ggT2(int x, int y) {
        int a = x;
        int b = y;

        if(x == 0) {
            return a;
        } else {
            while (b != 0)  {
                if (a > b) {
                    //System.out.println("a" + a);
                    if (ggT2(a - b, b) != 0) {
                        a = ggT2(a - b, b);
                        return a;
                    } else {
                        break;
                    }
                } else {
                    //System.out.println("b" + b);
                    if (ggT2(a,b -a) != 0) {
                        b = ggT2(a,b -a);
                        return b;
                    } else {
                        break;
                    }
                }
            }
            return b;
        }
    }
    public static void ggT3(int x, int y) {
        if(x == 0) {
            System.out.println(y);
        } else {
            while (y != 0)  {
                if (x > y) {
                    x = x % y;
                } else {
                    y = y % x;
                }
            }
            System.out.println(x);
        }
    }
    public static int ggT4(int x, int y) {
        int a = x;
        int b = y;
        if(a == 0) {
            return a;
        } else {
            while (b != 0)  {
                if (a > b) {
                    if (ggT4(a % b, b) != 0) {
                        return ggT4(a % b, b);
                    }
                } else {
                    if (ggT4(a, b % a) != 0) {
                        return ggT4(a, b % a);
                    }
                }
            }
            System.out.println(x);
        }
        return b;
    }
    public static void main(String[] args) {
        int x = Integer.parseInt(args[0]);
        int y = Integer.parseInt(args[1]);

        System.out.print("Der ggT von " + x + " und " + y + " ist ");
        ggT1(x, y);
        System.out.println(ggT2(x, y));
        ggT3(x, y);
        System.out.println(ggT4(x, y));
        //ggT3(x, y);
    }
}
