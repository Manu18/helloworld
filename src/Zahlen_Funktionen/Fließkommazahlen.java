package Zahlen_Funktionen;

public class Fließkommazahlen {
    public static void main(String[] args) {
        float x = 2304f;
        float y = -4096f;
        float z = 4096.001953125f;
        float ergebnis1 = x*(y+z);
        float ergebnis2 = x*y + x*z;
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
        System.out.println(ergebnis1);
        System.out.println(ergebnis2);
    }
}
