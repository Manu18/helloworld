package Zahlen_Funktionen;

public class Zufall {

    public static void main(String[] args) {
        double x = Double.parseDouble(args[0]);
        double y = Double.parseDouble(args[1]);
        double e = Double.parseDouble(args[2]);

        double a = Math.random();
        double b = Math.random();

        if ((e > 0) && (x < y) &&  ((y - x) > e)) {
            while (Math.abs(a - b) >= e ) {
                a = Math.random();
                b = Math.random();
                System.out.print(a);
                System.out.print(b);
            }
        } else {
            System.out.println("Falsche Grundgerüste.Eingabe!");
        }
        System.out.println(" \n \n \n");
        System.out.println("Zufallszahlen: " + a + " " + b);

    }
}
