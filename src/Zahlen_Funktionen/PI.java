package Zahlen_Funktionen;

public class PI {
    public static void main(String[] args) {
        double zähler = -1.0;
        double nenner = 0.0;
        double ergebnis = 0.0;
        double ergebnis2 = 0.0;
        double pi = Math.PI;

        for (int i = 0; i < 100000; i++) {
            zähler *= -1;
            nenner = 2*i + 1;
            ergebnis += (zähler / nenner);
        }
        System.out.println("PI ist: " + ergebnis * 4);
        zähler = -1;
        double k = 0;
        do {
            zähler *= -1;
            nenner = 2*k + 1;
            ergebnis2 += (zähler / nenner);
            k++;
        } while (Math.abs((ergebnis2 * 4) - pi) >= 0.0000001);

        System.out.println("PI ist: " + ergebnis2 * 4);
        System.out.println("PI ist: " + pi);

    }
}
