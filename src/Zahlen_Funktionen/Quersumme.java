package Zahlen_Funktionen;

public class Quersumme {
	public static void main(String[] args) {
		String eingabe = args[0];
		int summe = quersumme(eingabe);
		System.out.println(summe);
	}
	public static int quersumme(String s) {
		int summe = 0;
		for (int i = 0; i < s.length(); i++) {
			summe += s.charAt(i) - '0';
		}
		return summe;
	}
}