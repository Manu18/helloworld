package Zahlen_Funktionen;

public interface ZahlInterface {
    public Zahl addieren(Zahl z);
    public Zahl subtrahieren(Zahl z);
    public Zahl multiplizieren(Zahl z);
    public Zahl dividieren(Zahl z);
    public String toString();
}
