package Zahlen_Funktionen;

public class KomplexeZahl extends Zahl {
    double a;
    double b;

    public KomplexeZahl(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public boolean istGleich(Zahl z) {
        boolean gleich = (Math.sqrt(Math.pow(a,2) + Math.pow(b,2)) - Math.sqrt(Math.pow(((KomplexeZahl) z).a,2)+ Math.pow(((KomplexeZahl) z).b,2))) < epsilon;
        return gleich;
    }

    @Override
    public Zahl addieren(Zahl z) {
        String s = z.toString();
        String zahlC = "";
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '+') {
                zahlC = s.substring(0,(i));
                break;
            }
        }
        String zahlD = "";
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '+') {
                zahlD = s.substring((i+1),(s.length()-2));
                break;
            }
        }
        double c = Double.valueOf(zahlC);
        double d = Double.valueOf(zahlD);

        Zahl addiert = new KomplexeZahl((this.a + c),(this.b + d));
        return addiert;    // (a+b∗i)+(c+d∗i) = (a+c)+(b+d)∗i
    }

    @Override
    public Zahl subtrahieren(Zahl z) {
        String s = z.toString();
        String zahlC = "";
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '+') {
                zahlC = s.substring(0,(i));
                break;
            }
        }
        String zahlD = "";
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '+') {
                zahlD = s.substring((i+1),(s.length()-2));
                break;
            }
        }
        double c = Double.valueOf(zahlC);
        double d = Double.valueOf(zahlD);

        Zahl subtrahiert = new KomplexeZahl((this.a - c),(this.b - d));
        return subtrahiert;     //(a+b∗i)−(c+d∗i) = (a−c)+(b−d)∗i
    }

    @Override
    public Zahl multiplizieren(Zahl z) {
        String s = z.toString();
        String zahlC = "";
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '+') {
                zahlC = s.substring(0,(i));
                break;
            }
        }
        String zahlD = "";
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '+') {
                zahlD = s.substring((i+1),(s.length()-2));
                break;
            }
        }
        double c = Double.valueOf(zahlC);
        double d = Double.valueOf(zahlD);

        Zahl multipliziert = new KomplexeZahl(((this.a*c) - (this.b*d)),((this.a*d) + (this.b*c)));
        return multipliziert;      // (a+b∗i)∗(c+d∗i) = (ac−bd)+(ad+bc)∗i
    }

    @Override
    public Zahl dividieren(Zahl z) {
        String s = z.toString();
        String zahlC = "";
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '+') {
                zahlC = s.substring(0,(i));
                break;
            }
        }
        String zahlD = "";
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '+') {
                zahlD = s.substring((i+1),(s.length()-2));
                break;
            }
        }
        double c = Double.valueOf(zahlC);
        double d = Double.valueOf(zahlD);

        double ausdruckA = (this.a*c) + (this.b*d);
        ausdruckA /= ((c*c) + (d*d));

        double ausdruckB = (this.b*c) - (this.a*d);
        ausdruckB /= ((c*c) + (d*d));

        Zahl dividiert = new KomplexeZahl(ausdruckA,ausdruckB);
        return dividiert;
    }

    @Override
    public String toString() {
        //String ausgabe = "(" + a + "+" + b + "*i)";
        String ausgabe = a + "+" + b + "*i";
        return ausgabe;
    }
}
