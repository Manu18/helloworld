package Zahlen_Funktionen;
public class ReelleZahl extends Zahl {
    double r;

    public ReelleZahl(double r) {
        this.r = r;
    }

    @Override
    public boolean istGleich(Zahl z) {
        boolean gleich = (Math.abs(this.r) - Math.abs(((ReelleZahl) z).r)) < epsilon;
        return gleich;
    }

    @Override
    public Zahl addieren(Zahl z) {
        Zahl addiert = new ReelleZahl(this.r + Double.valueOf(z.toString()));
        return addiert;
    }

    @Override
    public Zahl subtrahieren(Zahl z) {
        Zahl subtrahiert = new ReelleZahl(this.r - Double.valueOf(z.toString()));
        return subtrahiert;
    }

    @Override
    public Zahl multiplizieren(Zahl z) {
        Zahl multipliziert = new ReelleZahl(this.r * Double.valueOf(z.toString()));
        return multipliziert;
    }

    @Override
    public Zahl dividieren(Zahl z) {
        Zahl dividiert = new ReelleZahl(this.r / Double.valueOf(z.toString()));
        return dividiert;
    }

    @Override
    public String toString() {
        return String.valueOf(r);
    }
}
