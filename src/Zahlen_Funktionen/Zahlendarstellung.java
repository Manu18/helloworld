package Zahlen_Funktionen;

import java.util.Arrays;

public class Zahlendarstellung {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(ermittleZiffern(0, 10)));
        System.out.println(Arrays.toString(ermittleZiffern(2, 1)));
        System.out.println(Arrays.toString(ermittleZiffern(2, 2)));
        System.out.println(Arrays.toString(ermittleZiffern(4, 2)));
        System.out.println(Arrays.toString(ermittleZiffern(100, 10)));
        System.out.println(Arrays.toString(ermittleZiffern(16, 32)));
        System.out.println(Arrays.toString(ermittleZiffern(4711, 16)));
        System.out.println(Arrays.toString(ermittleZiffern(4711, 8)));
        System.out.println(Arrays.toString(ermittleZiffern(4711, 2)));
        System.out.println(Arrays.toString(ermittleZiffern(1234, 16)));
    }

    public static int[] ermittleZiffern(int x, int b) {
        boolean unfertig = true;
        int benoetigeFeldlaenge;

        if(b == 1) {
            int[] resultat = new int[x];
            for (int i = 0; i < x; i++) {
                resultat[i] = 1;
            }
            return resultat;
        } else {

            if (b <= x) {
                double k = 0;
                int f = 0;
                while (k <= x) {
                    k = Math.pow(b, f);
                    if (k <= x) {
                        f++;
                    }
                }
                benoetigeFeldlaenge = f;
            } else {
                benoetigeFeldlaenge = 1;
            }

            if (x == 0) {
                int[] resultat = new int[1];
                resultat[0] = 0;
                return resultat;
            } else {
                int[] resultat = new int[benoetigeFeldlaenge];
                int k = 0;
                while (unfertig) {
                    resultat[k] = x % b;
                    x /= b;
                    k++;
                    if (x < 1) {
                        unfertig = false;
                    }
                }
                return resultat;
            }

        }
    }
}
