package Zahlen_Funktionen;

public class Statistik {
    public static void main(String[] args) {
        int[] x = {1,2,3,3,4,5};
        System.out.println("arithmetischer Mittelwert: " + arithMittelwert(x));
        System.out.println("harmonischer Mittelwert: " + harmMittelwert(x));
        System.out.println("Standardabweichung: " + abweichung(x));

    }

    public static double arithMittelwert(int[] x) {
        if (x.length == 0) {
            return 0.0;
        } else {
            double mittelwert = 0;
            for (int i = 0; i < x.length; i++) {
                mittelwert += x[i];
            }
            mittelwert /= x.length;
            return mittelwert;
        }
    }
    public static double harmMittelwert(int[] x) {
        double summe = 0.0;
        for (int i = 0; i < x.length; i++) {
            summe += 1.0 / x[i];
        }
        if (summe != 0) {
            return x.length / summe;
        } else {
            return 0.0;
        }
    }
    public static double abweichung(int[] x) {
        double mittelwert = arithMittelwert(x);
        double summe = 0.0;
        for (int i = 0; i < x.length; i++) {
            double summand = x[i] - mittelwert;
            summe += summand * summand;
        }
        if (x.length > 1) {
            return Math.sqrt(summe / (x.length - 1));
        } else {
            return 0.0;
        }
    }
}
