package Zahlen_Funktionen;

public class ZahlenTest {
    public static void main(String[] args) {
        Zahl z1 = new ReelleZahl(3.0);
        Zahl z2 = new ReelleZahl(5.0);
        System.out.println(z1.addieren(z2));
        System.out.println(z1.multiplizieren(z2));
        System.out.println(z1.dividieren(z2));
        System.out.println(z1.subtrahieren(z2));
        // Ausgabe muss 15.0 sein
        z1 = new KomplexeZahl(-1.0,2.5);
        z2 = new KomplexeZahl(-5.9,9.1);

        //-5.9+9.1*i  -1.0+2.5*i
        System.out.println(z1.addieren(z2));
        System.out.println(z1.multiplizieren(z2));
        System.out.println(z1.dividieren(z2));
        System.out.println(z1.subtrahieren(z2));
        // Ausgabe muss 14.0+8.0∗ i sein
    }
}
