package Zahlen_Funktionen;

public interface Vergleichen {
    final double epsilon = Math.E - 12;
    public boolean istGleich(Zahl z);
}
