package Zahlen_Funktionen;

import java.util.Arrays;

public class Potenz {
    public static void main(String[] args) {
        System.out.println(potenz(2,8));
        System.out.println(Math.pow(2,8));
        System.out.println(Arrays.toString(abc(-3,5,1)));
    }

    public static int potenz(int x, int y) {
        if(y > 1) {
            x *= potenz(x,y-1);
        }
        return x;
    }

    public static double[] abc(double a, double b, double c) {
        double erg[] = new double[2];
        double wurzel = Math.sqrt((b*b)-(4*a*c));
        double x1 = (-b + wurzel) / 2*a;
        double x2 = (-b - wurzel) / 2*a;
        erg[0] = x1;
        erg[1] = x2;
        return erg;
    }
}
