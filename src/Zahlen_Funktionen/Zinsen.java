package Zahlen_Funktionen;

public class Zinsen {

    public static double einfacheVerzinsung(double startkapitalP, double zinsfußP, double jahreP) {
        double endkapital;

        double startkapital = startkapitalP;
        double zinsfuß = zinsfußP;
        double jahre = jahreP;
        double zinssatz = zinsfuß / 100f;

        endkapital = startkapital * (1f + (zinssatz * jahre));
        return endkapital;
    }

    public static double zinsesZins(double startkapitalP, double zinsfußP, double jahreP) {
        double endkapital;
        double zwischenergebnis;

        double startkapital = startkapitalP;
        double zinsfuß = zinsfußP;
        double jahre = jahreP;
        double zinssatz = zinsfuß / 100f;

        zwischenergebnis = Math.pow((1f + zinssatz),jahre);
        endkapital = startkapital * zwischenergebnis;
        return endkapital;
    }

    public static double effektiveVerzinsung(double startkapitalP, double zinsfußP, double jahreP, double verzinsungenP) {
        double endkapital;
        double zwischenergebnis;

        double startkapital = startkapitalP;
        double zinsfuß = zinsfußP;
        double zinssatz = zinsfuß / 100f;
        double jahre = jahreP;
        double verzinsungen = verzinsungenP;

        zwischenergebnis = Math.pow((1f + (zinssatz / verzinsungen)), (verzinsungen * jahre));
        endkapital = startkapital * zwischenergebnis;
        return endkapital;
    }


    public static void main(String[] args) {
        double K0 = Double . parseDouble ( args [0]);
        double p = Double . parseDouble ( args [1]);
        double n = Double . parseDouble ( args [2]);
        double m = Double . parseDouble ( args [3]);

        System.out.println(einfacheVerzinsung(K0,p,n));
        System.out.println(zinsesZins(K0,p,n));
        System.out.println(effektiveVerzinsung(K0,p,n,m));

    }
}
