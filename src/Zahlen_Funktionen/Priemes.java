package Zahlen_Funktionen;
import java.util.*;
public class Priemes {
	public static void main(String[] args) {
		//int a = Integer.parseInt(args[0]);
		int a = 7;
		long timeStart = System.currentTimeMillis();
		System.out.println(Arrays.toString(primes(a)));
		long timeStop = System.currentTimeMillis();
		System.out.println(timeStart);
		System.out.println(timeStop);
		System.out.println(timeStop - timeStart);
	
	}
	
	public static int[] primes(int n) {
		int[] primes = new int[n-1];
		int zahlen = n;
		for (int i = 2; i <= n; i++) {
			primes[i - 2] = i;
		
		}
		for (int i = 0; i < primes.length; i++) {
			for (int j = i + 1; j < primes.length; j++) {
				if ((primes[i] != 0) && (primes[j] != 0) && (primes[j] % primes[i] == 0)) {
					primes[j] = 0;
					zahlen--;
				}
			
			}	
		}
		
		int[] p = new int[zahlen - 1];
		int k = 0;
		System.out.println(Arrays.toString(primes));
		for (int i = 0; i < primes.length; i++) {
			if (primes[i] != 0) {
				p[k] = primes[i];
				k++;
			}
		}
		return p;
		
	}
}