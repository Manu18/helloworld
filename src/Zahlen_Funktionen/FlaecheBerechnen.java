package Zahlen_Funktionen;

public class FlaecheBerechnen {
    public static float flaecheBerechnen(float[][] coord) {
        float fläche = 0;
        int n = coord.length;
        float summe1 = 0;
        float summe2 = 0;

        for (int i = 0; i < n; i++) {
            summe1 = (coord[i][0] + coord[(i + 1) % n][0]);
            summe2 = (coord[(i + 1) % n][1] - coord[i][1]);
            fläche += (summe1 * summe2);
        }
        fläche = Math.abs(fläche / 2f);
        return fläche;
    }

    public static void main(String[] args) {

    }
}
