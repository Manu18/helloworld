public class MatrixTr {
	public static void main(String[] args) {
		int[][] a = {{1,2,3,4},{1,2,3,4},{1,2,3,4}};
		int[][] c = transponieren(a);
		ausgabe(a);
		System.out.println();
		ausgabe(c);
	
	}
	public static int[][] transponieren(int[][] a) {
		int[][] c = new int[a[0].length][a.length];
		for(int i = 0; i < a.length; i++) {
			for(int j = 0; j < a[0].length; j++) {
				c[j][i] = a[i][j];
			}
		}
		return c;
	}
	public static void ausgabe(int[][] a) {
		for(int i = 0; i < a.length; i++) {
			for(int j = 0; j < a[0].length; j++) {
				System.out.print(a[i][j]);
			}
			System.out.println();
		}
	}
}