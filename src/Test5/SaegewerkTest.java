package Test5;

import java.util.Scanner;

public class SaegewerkTest {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[][] balken = new int[n][2];
        for (int i = 0; i < n; i++) {
            balken[i][0] = s.nextInt();
            balken[i][1] = s.nextInt();
        }
        int m = s.nextInt();
        int[][] staemme = new int[m][2];
        for (int i = 0; i < m; i++) {
            staemme[i][0] = s.nextInt();
            staemme[i][1] = s.nextInt();
        }
        schneiden(balken, staemme);
    }

    public static void schneiden(int[][] balken, int[][] staemme) {
        int fertigeBalken = 0;
        for (int k = fertigeBalken; (k < balken.length); k++) {
            for (int i = 0; i < staemme.length; i++) {
                if ((balken[k][0] <= staemme[i][0]) && (balken[k][1] <= staemme[i][1])) {
                    int rest = staemme[i][0] - balken[k][0];
                    int abgesaegt = balken[k][0];
                    String ausgabe = "Von Stamm " + (i + 1) + " wurde " + abgesaegt + " abgeschnitten. Restlaenge ist " + rest;
                    System.out.println(ausgabe);
                    staemme[i][0] = rest;
                    fertigeBalken++;
                    break;
                }
            }
        }
    }
}
