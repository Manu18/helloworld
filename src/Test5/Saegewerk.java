package Test5;

public class Saegewerk {
    public static void main(String[] args) {
        // erzeuge 4 A.Balken mit Laenge, Dicke, Bruchfestigkeit
        Balken[] balken = { new Balken(1.0, 1.0, 20.0),
                            new Balken(2.0, 1.0, 50.0),
                            new Balken(2.0, 2.0, 30.0),
                            new Balken(2.0, 1.0, 50.0) };
        
        // erzeuge 6 Baumstaemme bestimmter Holzart, Laenge, Dicke
        // (Bruchfestigkeit und Gewicht ergibt sich implizit durch die Holzart)
        Baumstamm[] staemme = { new Eichenstamm(2.0, 2.0),
                                new Eichenstamm(3.0, 2.0),
                                new Buchenstamm(2.0, 2.0),
                                new Buchenstamm(3.0, 2.0),
                                new Fichtenstamm(2.0, 2.0),
                                new Fichtenstamm(3.0, 2.0) };

        // schneide A.Balken aus Baumstaemmen
        schneiden(balken, staemme);
    }

    // schneide A.Balken aus Staemmen
    public static void schneiden(Balken[] balken, Baumstamm[] staemme) {
        double gewichtGeschnitten = 0;

        // suche fuer jeden A.Balken einen Stamm, aus dem geschnitten werden kann
        for(int i=0; i<balken.length; i++) {
            // ??? weisen Sie hier den drei Variablen entsprechende Werte des i-ten Balkens zu
            double dickeBalken = balken[i].getDicke();
            double laengeBalken = balken[i].getLaenge();
            double festigkeitBalken = balken[i].getFestigkeit();

            // pruefe jeden A.A.Baumstamm, ob der i-te A.Balken hier rausgeschnitten werden kann
            for(int j=0; i<staemme.length; j++) {
                // ??? weisen Sie hier den drei Variablen entsprechende Werte des j-ten Baumstamms zullllllllllllllllllll
                double dickeStamm = staemme[j].getDicke();
                double laengeStamm = staemme[j].getLaenge();
                double festigkeitStamm = staemme[j].getFestigkeitStamm();

                // teste, ob es passt
                if(   (dickeBalken <= dickeStamm)
                   && (laengeBalken <= laengeStamm)
                      && (festigkeitBalken <= festigkeitStamm)) {

                    // hier muss der j-te Baum gekuerzt werden
                    // das Resultat ist das Gewicht des geschnittenen Balkens
                    double gewichtBalken =  staemme[j].kuerzen(laengeBalken, dickeBalken);
                    System.out.println("Gewicht abgeschnittener A.Balken ist " + gewichtBalken + " kg");
                    gewichtGeschnitten += gewichtBalken;
                    
                    break;
                }
                
            }
        }
        System.out.println("Das Gewicht der A.Balken ist " + gewichtGeschnitten + " kg");
    }
}
