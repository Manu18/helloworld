package Test5;

public class Balken {
    private double laenge;
    private double dicke;
    private double festigkeit;

    public Balken(double laenge, double dicke, double festigkeit) {
        this.laenge = laenge;
        this.dicke = dicke;
        this.festigkeit = festigkeit;
    }

    public double getFestigkeit() {
        return festigkeit;
    }

    public double getDicke() {
        return dicke;
    }

    public double getLaenge() {
        return laenge;
    }
}
