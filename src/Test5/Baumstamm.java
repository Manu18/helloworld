package Test5;

public class Baumstamm {
    private double laenge;
    private double dicke;
    private double festigkeit;
    private double gewicht;

    public Baumstamm(double laenge, double dicke, double festigkeit, double gewicht) {
        this.laenge = laenge;
        this.dicke = dicke;
        this.festigkeit = festigkeit;
        this.gewicht = gewicht;
    }

    public double kuerzen(double laenge, double dicke) {
        Balken balken = new Balken(laenge, dicke, this.festigkeit);
        this.laenge -= laenge;
        //double gewichtB = (Math.PI * Math.pow((dicke / 2),2)) * laenge * this.gewicht;
        double gewichtB = dicke * laenge * this.gewicht;
        return gewichtB;
    }


    public double getLaenge() {
        return laenge;
    }

    public double getDicke() {
        return dicke;
    }
    public double getFestigkeitStamm() {
        return festigkeit;
    }
}
