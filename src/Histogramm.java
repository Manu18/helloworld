public class Histogramm {
    public static void main(String[] args) {
        int[] eingabe = {1,1,1,2};
        int nullen = 0;
        int eins = 0;
        int zwei = 0;
        int drei = 0;
        int vier = 0;
        int fünf = 0;
        for (int i = 0; i < eingabe.length; i++) {
            switch (eingabe[i]) {
                case 0:
                    nullen++;
                    break;
                case 1:
                    eins++;
                    break;
                case 2:
                    zwei++;
                    break;
                case 3:
                    drei++;
                    break;
                case 4:
                    vier++;
                    break;
                case 5:
                    fünf++;
                    break;
            }
        }
        int[] z = {nullen, eins, zwei, drei, vier, fünf};
        System.out.print("0: ");
        ausgabe(0, nullen, z);
        System.out.print("1: ");
        ausgabe(1, eins, z);
        System.out.print("2: ");
        ausgabe(2, zwei, z);
        System.out.print("3: ");
        ausgabe(3, drei, z);
        System.out.print("4: ");
        ausgabe(4, vier, z);
        System.out.print("5: ");
        ausgabe(5, fünf, z);
    }


    public static void ausgabe(int aktuelleZahl, int sterne, int[] z) {
        int ursprungsSterne = sterne;
        float sternchen = sterne;
        if (sterne == größteZahl(z)) {
            anzahlSterne = 10;
            System.out.print("**********  (" + ursprungsSterne + "," + anzahlSterne + ")\n");
            anzahlSterne = 0;
        } else {
            int größteZahl = größteZahl(z);
            sternchen = ( (sternchen / größteZahl)) * 10;
            for (int i = 1; i <= sternchen; i++) {
                System.out.print("*");
                anzahlSterne = i;
            }
            System.out.print("  (" + ursprungsSterne + "," + anzahlSterne + ")\n");
            anzahlSterne = 0;
        }
    }

    public static int häufigsteZahl;
    public static int anzahlSterne;

    public static int größteZahl(int[] z) {
        int größteZahl = 0;
        for (int i = 0; i <= 5; i++) {
            if (z[i] > größteZahl) {
                größteZahl = z[i];
                häufigsteZahl = i;
            }
        }
        return größteZahl;
    }
}
