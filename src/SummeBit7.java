public class SummeBit7 {
    public static void main(String[] args) {

        int n = Integer.parseInt(args[0]);
        int summe = 0;

        for (int i = n; i <= Math.pow(n, 2); i++) {
            if (((i >> 7) & 0b1) == 1) {
                System.out.println("Zahl mit 7.Bit=1: " + i);
                summe += i;
            }
        }
        System.out.println("Summe=" + summe);

    }
}