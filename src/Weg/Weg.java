package Weg;

public class Weg {
    Punkt[] punkte;
    private int wegLaenge = 0;

    Weg(int x, int y) {
        this.wegLaenge = 1;
        Punkt p = new Punkt(x,y);
        punkte = new Punkt[1];
        this.punkte[0] = p;
    }

    public int getAnzahl() {
        return this.wegLaenge;
    }

    public void verlaengern(Weg w) {
        Punkt[] neuerWeg = new Punkt[this.wegLaenge + w.wegLaenge];

        // eigenen Punkte eintragen
        for (int i = 0; i < this.wegLaenge; i++) {
            neuerWeg[i] = this.punkte[i];
        }

        // punkte von w eintragen
        int k = 0;
        for (int i = this.wegLaenge; i < neuerWeg.length; i++) {
            neuerWeg[i] = w.punkte[k];
            k++;
        }
        this.wegLaenge += w.wegLaenge;
        this.punkte = neuerWeg;
    }

    public String toString() {
        String ausgabe = "";
        for (int i = 0; i < wegLaenge; i++) {
            ausgabe += this.punkte[i].toString();
            if (wegLaenge > 1 && i != wegLaenge - 1) {
                ausgabe += "-";
            }
        }
        return ausgabe;
    }

}
