package Weg;

public class Punkt {
    public int x;
    public int y;

    Punkt(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return "(" + this.x + "," + this.y + ")";
    }


}
