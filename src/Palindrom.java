import java.util.*;

public class Palindrom {
	public static void main(String[] args) {
		//Scanner s = new Scanner(System.in);
		//String s = s.next();
		String s = "abcdefghijklmnop";
		System.out.println(palindrom(s));
	}

	public static String palindrom(String s) {
		if(s.length() == 1) {
			return s + s;
		} else {
			return s.charAt(0) + palindrom(s.substring(1)) + s.charAt(0);
		}
	}
}