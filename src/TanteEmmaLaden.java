import java.util.*;

public class TanteEmmaLaden {
	public static void main(String[] args) {
		System.out.println("Geben Sie die Anzahl gefolgt von dem Artikel (Gummibaerchen, Lakritzestangen, Playstation) ein:");
		Scanner s = new Scanner(System.in);
		int anzahl = s.nextInt();
		String artikel = s.next(); 
		s.close();
		System.out.println(ausgabe(anzahl, artikel));
	}
	
	public static String ausgabe(int anzahl, String artikel) {
		String ausgabe = "";
		if (artikel.equals("Gummibaerchen")) {
			double brutto = 0.05;
			double netto = brutto / 1.07;
		
			ausgabe = "Netto ohne MwSt: " + netto * anzahl + "\n";
			ausgabe += "MwSt: " + anzahl * (netto * 0.07) + "\n";
			ausgabe += "Zu Bezahlen: " + anzahl * brutto;
		}else if (artikel.equals("Lakritzestangen")) {
			double brutto = 0.30;
			double netto = brutto / 1.07;
			
			ausgabe = "Netto ohne MwSt: " + netto * anzahl + "\n";
			ausgabe += "MwSt: " + anzahl * (netto * 0.07) + "\n";
			ausgabe += "Zu Bezahlen: " + anzahl * brutto;
		} else if (artikel.equals("Playstation")) {
			double brutto = 199;
			double netto = brutto / 1.19;
			
			ausgabe = "Netto ohne MwSt: " + netto * anzahl + "\n";
			ausgabe += "MwSt: " + anzahl * (netto * 0.19) + "\n";
			ausgabe += "Zu Bezahlen: " + anzahl * brutto;
		} else {
			ausgabe = "Fehler: Falsche Grundgerüste.Eingabe";
		}
		return ausgabe;
	} 
}