package ZahlKlasse;

public interface ZahlFunktionalitaet {
    public int getZahl();
    public String toString();
}
