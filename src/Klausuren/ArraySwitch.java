package Klausuren;

import java.util.*;

public class ArraySwitch {
	public static void main(String[] args) {
		int[] a = {1,2,3,5,3,8,5};
		int[] b = {6,5,4,3,2,9};
		double[][] c = {{1d,2d,3d},{2d,3d,4d},{4d,5d,6d}};
		System.out.println(Arrays.toString(a));
		System.out.println(Arrays.toString(b));
		arraySwitch(a,b);
		System.out.println(Arrays.toString(a));
		System.out.println(Arrays.toString(b));
		double k = spaltensumme(c,0);
		System.out.println(k);
	}

	public static void arraySwitch(int[] a, int[] b) {
		int l = b.length;
		if (a.length <= b.length) {
			l = a.length;
		}
		for(int i=0; i < l; i++) {
			if(a[i] >= b[i]) {
				int c = a[i];
				a[i] = b[i];
				b[i] = c;
			}
		}
	}
	
	public static double spaltensumme(double[][] a, int k) {
		if(k > a[0].length) {
			return 0d;
		}
		double sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i][k];
		}
		return sum;
	}
}