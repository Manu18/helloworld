package Klausuren;

public class Palindrom {
	public static void main(String[] args) {
		//Scanner s = new Scanner(System.in);
		//String s = s.next();
		String s = "abc";
		System.out.println(palindrom(s));
	}

	public static String palindrom(String s) {
		String f = s;
		if(s.length() == 2) {
			return s;
		} else {
			f += "" + s.charAt(s.length()-1) + palindrom(s.substring(1,s.length()));
		}
		return f;
	}
}