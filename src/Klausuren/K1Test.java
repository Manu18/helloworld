package Klausuren;

    interface IFace {
        public void f(float g);
    }

    class K1 implements IFace {
        public void f(float g) {
            System.out.print("K1−f, ");
            g();
        }
        void g() {
            System.out.print("K1−g, ");
            m();
            h();
        }
        static void h() {
            System.out.println("K1−h");
        }
        private void m() {
            System.out.println("hi K1!!!!!");
        }
    }

    class K2 extends K1 {
        public void f(float g) {
            System . out.print("K2−f, ");
            g();
        }
        static void h() {
            System.out.println("K2−h");
        }
        void m() {
            System.out.println("hi k2");
        }
    }
    public class K1Test {
        public static void main(String[] args) {
            K1 k1 = new K1();
            k1.f(1.0F);
            K2 k2 = new K2();
            k2.f(2.0F);
            ((K1) k2).f(3.0F);
            IFace ik = (IFace) k2;
            ik.f(4.0F);
     //       ((K1) k2).m();
        }
    }
