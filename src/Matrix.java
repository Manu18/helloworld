public class Matrix {
	public static void main(String[] args) {
		int[][] a = {{3,1},{2,0},{1,2}};
		int[][] b = {{1,0,4},{2,1,0}};
		
		int[][] c = matrixMult(a,b);
	}
	
	public static int[][] matrixMult(int[][] a, int[][] b) {
		int[][] c = new int[a[0].length][b.length];
		
		for(int i = 0; i < c.length; i++) {
			for(int j = 0; j < c[0].length; j++) {
				// additon
				int code = 0;
				for (int k = 0; k < a.length; k++) {
					code += a[k][i] * b[j][k];
					//System.out.println(a[k][i] + "*" + b[j][k]);
				}
				c[i][j] = code;
				System.out.print(code + " ");
			}
			System.out.println();
		}
		return c;
	}


}