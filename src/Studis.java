public class Studis {
    double lernfortschritt;
    boolean gruppenmitglied;
    int gruppennummer;
    double lerngruppenfortschritt;

    public Studis() {
        this.lernfortschritt = 0;
        this.gruppenmitglied = false;
    }

    public void ersteHaelfte(Studis[] studenten) {
        for (int i = 0; i < 15; i++) {
            this.vorlesung();
            this.nacharbeitung();
            this.uebung();
            this.gruppenarbeit(studenten);
        }
    }

    public void zweiteHaelfte(Studis[] studenten) {
        for (int i = 0; i < 15; i++) {
            this.gruppenarbeit(studenten);
            if (i % 2 == 0) {
                this.vorlesung();
                this.nacharbeitung();
                this.uebung();            this.vorlesung();
                this.nacharbeitung();
                this.uebung();
            }
        }
    }

    public static void gruppenmitglieder(Studis[] studenten) {
        int counter = 0;
        int gruppe = 0;
        for (int i = 0; i < studenten.length; i++) {
            if (i % 2 == 0) {
                counter++;
                if (counter > 3) {
                    gruppe++;
                    counter = 0;
                }
                studenten[i].gruppenmitglied = true;
                studenten[i].gruppennummer = gruppe;
            }
        }
    }

    public void vorlesung() {
        this.lernfortschritt += 0.1;
    }

    public void nacharbeitung() {
        this.lernfortschritt += 0.2;
    }

    public void uebung() {
        this.lernfortschritt += 0.3;
    }

    public void gruppenarbeit(Studis[] studenten) {
        double zusatz = 0;
        double hoechstwert = 0d;
        this.lerngruppenfortschritt = this.lernfortschritt;
        if (this.gruppenmitglied) {
            for (int i = 0; i < studenten.length; i++) {
                if ((this.gruppennummer == studenten[i].gruppennummer) && (studenten[i].lerngruppenfortschritt > hoechstwert)) {
                    hoechstwert = studenten[i].lerngruppenfortschritt;
                }
            }
            if (this.lerngruppenfortschritt == hoechstwert) {
                zusatz = 0.5;
            } else {
                zusatz = 1 / ((hoechstwert - this.lerngruppenfortschritt)) * 10;
            }
            this.lernfortschritt += zusatz;
        }
    }

    public static Studis[] neueStudis(int anzahl) {
        Studis[] studenten = new Studis[anzahl];
        for (int i = 0; i < anzahl; i++) {
            studenten[i] = new Studis();
        }
        gruppenmitglieder(studenten);
        return studenten;
    }

}
