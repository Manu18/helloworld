package Fußball;

public class Stadion {
    public static void main(String[] args) {
        Fussballspieler[] team1 = new Fussballspieler[5];
        Fussballspieler[] team2 = new Fussballspieler[5];
        for (int i = 0; i < 5; i++) {
            String name1 = "Spieler-rot-" + (i+1);
            String name2 = "Spieler-blau-" + (i+1);
            int einkommen1 = 10000 * (i+1);
            int einkommen2 = 20000 * (i+1);
            team1[i] = new Fussballspieler(name1, einkommen1);
            team2[i] = new Fussballspieler(name2, einkommen2);
        }

        Mannschaft rot = new Mannschaft(team1[0],team1[1],team1[2],team1[3],team1[4]);
        Mannschaft blau = new Mannschaft(team2[0],team2[1],team2[2],team2[3],team2[4]);

        Person[] zuschauer = new Person[45000];
        for (int i = 0; i < zuschauer.length; i++) {
            String name = "Zuschauer-" + (i+1);
            zuschauer[i] = new Person(name);
        }
        String team1S = "";
        String team2S = "";
        for (int i = 0; i < 5; i++) {
            team1S += (i+1) + ". " + team1[i].getName() + "\n";
            team2S += (i+1) + ". " + team2[i].getName() + "\n";
        }

        String ausgabe = team1S + "\n" + team2S + "\n" + rot.einkommen() + "\n" + blau.einkommen() + "\n" + zuschauer[0].getName();
        System.out.println(ausgabe);
    }
}
