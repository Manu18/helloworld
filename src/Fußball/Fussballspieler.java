package Fußball;

public class Fussballspieler extends Person {
    private int einkommen;
    Fussballspieler(String name, int einkommen) {
        super(name);
        this.einkommen = einkommen;
    }

    public int getEinkommen() {
        return this.einkommen;
    }

    public String toString() {
        return this.getName() + " " + this.getEinkommen();
    }

}
