package Fußball;

public class Mannschaft {
    Fussballspieler[] spieler = new Fussballspieler[5];

    Mannschaft(Fussballspieler s1, Fussballspieler s2, Fussballspieler s3, Fussballspieler s4, Fussballspieler s5) {
        spieler[0] = s1;
        spieler[1] = s2;
        spieler[2] = s3;
        spieler[3] = s4;
        spieler[4] = s5;
    }

    public int einkommen() {
        int einkommen = 0;
        for (int i = 0; i < 5; i++) {
            einkommen += spieler[i].getEinkommen();
        }
        return einkommen;
    }

    public String toString() {
        String ausgabe = "";
        for (int i = 1; i <= 5; i++) {
            ausgabe += i + ". " + spieler[i-1].getName() + "\n";
        }
        return ausgabe;
    }
}
