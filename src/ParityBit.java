public class ParityBit {
    public static void main(String[] args) {
        int wert = 0x17;
        int quersumme = 0;
        int parityBit = wert & 0x1;
        int bitWert = wert & 0x1;
        wert = wert >> 1;

        for (int i = 0; i < 4; i++) {
            bitWert = wert & 0x1;
            quersumme += bitWert;
            wert = wert >> 1;
        }
        boolean uneven = quersumme % 2 == 1;
        boolean parity = false;
        if (parityBit == 0) {
            parity = false;
        } else if (parityBit == 1) {
            parity = true;
        }

        if (uneven && parity) {
            System.out.println("true");
        } else if ((uneven == false) && (parity == false)){
            System.out.println("true");
        } else {
            System.out.println("false");
        }
        System.out.println(quersumme);
        System.out.println(parityBit);


    }
}
