public class IntStack {
    static int belegt;
    int maximalGroesse;
    int sp;
    int[] a;

    IntStack(int n) throws Fehlermeldung {
        if (n <= 0) {
            Fehlermeldung n0 = new Fehlermeldung("Falsche Grundgerüste.Eingabe, maximaler Stack kann nicht 0 oder negativ sein");
            throw n0;
        }
        maximalGroesse = n;
        sp = -1;
        a = new int[n];
        if (belegt + (n / 4) >= Integer.MAX_VALUE) {
            Fehlermeldung n8 = new Fehlermeldung("Zu groß. Belegter Speicher kann nicht mit Int dargestellt werden");
            throw n8;
        }
        belegt += n * 4;
        assert ((a == null) && (sp == 0) && (maximalGroesse == 0)):"Stack Fehler: nicht angelegt";
    }

    public boolean isemptystack() {
        return (sp == -1);
    }

    public void push(int x) throws Fehlermeldung {
        if ((sp + 1) == maximalGroesse) {
            Fehlermeldung voll = new Fehlermeldung("Stack ist bereits voll");
            throw voll;
        }
        a[++sp] = x;
    }

    public int pop() throws Fehlermeldung {
        if (isemptystack()) {
            Fehlermeldung leer = new Fehlermeldung("Stack ist bereits leer");
            throw leer;
        }
        return a[sp--];
    }
    public static int gibSpeicherverbrauch() {
        assert (belegt >= 0) : "Belegt falsch";
        return belegt;
    }

    public static void main(String[] args) {
        try {
            IntStack s1 = new IntStack(15);
            IntStack s2 = new IntStack(12);
            assert (belegt != 0) : "komisch";
            for (int i = 0; i <= 11; i++) {
                s1.push(i);
                s2.push(i);
            }
            System.out.println(s1.isemptystack());
            for (int i = 0; i <= 5; i++) {
                System.out.println(s1.pop());
                s1.pop();
                s2.pop();
            }
            System.out.println(s1.isemptystack());
        } catch (Fehlermeldung fehlermeldung) {
            fehlermeldung.getMessage();
            fehlermeldung.printStackTrace();
        }
    }
}
