package Fehler;

public class Fehler1 extends RuntimeException {

    public Fehler1() {
        super("Fehler 1 aufgetreten");
    }
    public Fehler1(String message) {
        super(message);
    }
}
