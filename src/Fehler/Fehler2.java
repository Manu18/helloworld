package Fehler;

public class Fehler2 extends Exception {
    public Fehler2() {
        super("Es kam zu einem Fehler. Zahl zu groß!");
    }

    public Fehler2(String message) {
        super(message);
    }

}
