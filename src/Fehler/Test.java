package Fehler;

public class Test {
    public static void main(String[] args) {
        try {
            int k = testen2();
            testen1(k);
        } catch (Fehler2 fehler2) {
            fehler2.printStackTrace();
        }

    }

    public static void testen1(int k) {
        try {
            int[] test = new int[10];
            if (k >= 10) {
                Fehler1 e = new Fehler1();
                throw e;
            }
            for (int i = 0; i < k; i++) {
                test[i] = i;
            }
        } catch (Fehler1 e) {
            e.getMessage();
            e.printStackTrace();
        }
    }

    public static int testen2() throws Fehler2 {
        int k = 13;
        if (k >= 100) {
            Fehler2 e = new Fehler2();
            throw e;
        }
        return k;
    }


}
