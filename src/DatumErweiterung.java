import java.util.Scanner;
public class DatumErweiterung {

    public static int monatsTage(int monat) {
        int langerMonat = 31;
        int kurzerMonat = 30;
        int februar = 28;
        int tageImMonat = 0;

        switch (monat) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                tageImMonat = langerMonat;
                break;
            case 2:
                tageImMonat = februar;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                tageImMonat = kurzerMonat;
                break;
        }
        return tageImMonat;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int j = sc.nextInt(); // Kalender.Jahr 1600≤ j ≤3000
        int m = sc.nextInt(); // Monat 1≤ m ≤12
        int t = sc.nextInt(); // Kalender.Tag 1≤ t ≤31
        int s = sc.nextInt(); // Stunde 0≤ s ≤23
        int m1 = sc.nextInt(); // Minute 0≤ m ≤59
        int m2 = sc.nextInt(); // Minuten zum Datum X 0≤m2 ≤40000
        int minutenAdd = 0;
        int stundenAdd = 0;
        int tageAdd = 0;
        int monateAdd = 0;
        int jahreAdd = 0;

        if ((m1 + m2) < 60) {
            minutenAdd = m1 + m2;
        }

        if ((m1 + m2) >= 60) { // Stundensprung
            minutenAdd = (m1 + m2) % 60;
            stundenAdd = (m1 + m2) / 60;

            if ((s + stundenAdd) >= 24) { // Tagesprung
                tageAdd = (s + stundenAdd) / 24;

                if ((t + tageAdd) > monatsTage(m) ) { // Monatssprung
                    monateAdd = (t + tageAdd) / monatsTage(m);

                    if ((m + monateAdd) > 12) { // Jahressprung
                        jahreAdd = (m + monateAdd) / 12;
                    }
                }
            }
        }

        j += jahreAdd;

        if ((t + tageAdd) < monatsTage(m)) {
            t += tageAdd;
        } else {
            t = (t + tageAdd) % monatsTage(m);
            // Hier bin ich dumm
        }
        if ((m + monateAdd) < 13) {
            m += monateAdd;
        } else {
            m = (m + monateAdd) % 12;
        }
        if ((s + stundenAdd) < 24) {
            s += stundenAdd;
        } else {
            s = (s + stundenAdd) % 24;
        }
        m1 = minutenAdd;
        System.out.println(j + " " + m + " " + t + " " + s + " " + m1);

    }
}
