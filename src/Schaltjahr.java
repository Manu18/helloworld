public class Schaltjahr {
    public static void main(String[] args) {
        // Beispiel fuer ein zu ueberpruefendes Kalender.Jahr
        int jahr = 2013;
        boolean istSchaltjahr;
        // /* Eine Jahrzahl ist durch 4 und nicht durch 100 teilbar oder sie ist durch 400 teilbar */
        for ( int i = 0; i <= 10; i++) {
            istSchaltjahr = ( (   (jahr % 4) == 0) && (!((jahr % 100) == 0))) || ((jahr % 400) == 0);
            System.out.println("Das Kalender.Jahr " + jahr + " ist ein Schaltjahr? " + istSchaltjahr);
            jahr++;
        }

    }
}