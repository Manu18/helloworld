package Test6;

public abstract class Baumstamm {
    abstract void kuerzen(double laenge);
    abstract double gewicht();
    abstract String getName();
}
