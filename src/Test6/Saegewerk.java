package Test6;

public class Saegewerk {

    public static void main(String[] args) {
        Saege stichsaege = new Stichsaege();
        Saege kettensaege = new Kettensaege();

        Baumstamm eiche = new Eichenstamm(5.0);
        Baumstamm buche = new Buchenstamm(7.0);

        stichsaege.saegen(eiche, 1.0);
        kettensaege.saegen(buche, 2.0);

        System.out.println("Eiche hat ein Restgewicht von " + eiche.gewicht());
        System.out.println("Buche hat ein Restgewicht von " + buche.gewicht());
    }
}
