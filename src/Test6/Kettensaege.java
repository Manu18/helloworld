package Test6;

public class Kettensaege extends Saege {
    private String name;

    public Kettensaege() {
        this.name = "A.Kettensaege";
    }

    public void saegen(Baumstamm stamm , double laenge) {
        System.out.println(stamm.getName() + " " + laenge + "m saegen mit " + getName());
        stamm.kuerzen(laenge);
    }

    public String getName() {
        return name;
    }
}
