package Test6;

public class Stichsaege extends Saege {
    private String name;

    public Stichsaege() {
        this.name = "A.Stichsaege";
    }

    public void saegen(Baumstamm stamm, double laenge) {
        System.out.println(stamm.getName() + " " + laenge + "m saegen mit " + getName());
        stamm.kuerzen(laenge);
    }

    public String getName() {
        return name;
    }
}
