package Test6;

public class Buchenstamm extends Baumstamm {
    private double laenge;
    private double dicke;
    private int gewicht;
    private String name;

    public Buchenstamm(double laenge) {
        this.laenge = laenge;
        this.dicke = 1;
        this.gewicht = 60;
        this.name = "Buche";
    }

    void kuerzen(double laenge) {
        this.laenge -= laenge;
    }

    double gewicht() {
        double gewicht = this.laenge * this.dicke * this.gewicht;
        return gewicht;
    }

    public String getName() {
        return name;
    }
}
