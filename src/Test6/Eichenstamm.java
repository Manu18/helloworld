package Test6;

public class Eichenstamm extends Baumstamm {
    private double laenge;
    private double dicke;
    private int gewwicht;
    private String name;

    public Eichenstamm(double laenge) {
        this.laenge = laenge;
        this.dicke = 1;
        this.gewwicht = 52;
        this.name = "Eiche";
    }

    void kuerzen(double laenge) {
        this.laenge -= laenge;
    }

    double gewicht() {
        double gewicht = this.laenge * this.dicke * this.gewwicht;
        return gewicht;
    }

    public String getName() {
        return name;
    }
}
