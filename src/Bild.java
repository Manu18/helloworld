public class Bild {
    public static void main(String[] args) {
        byte[][] bild1 = {{1,1,1,1},{2,2,2,2},{3,3,3,3}};
        ausgabe(bild1);
        System.out.println();
        byte[][] bild2 = {{4,4,4},{5,5,5},{6,6,6},{7,7,7}};
        ausgabe(bild2);
        System.out.println();
        byte[][] res = ueberlagern(bild1, bild2);
        ausgabe(res);
    }

    public static byte[][] ueberlagern(byte[][] bild1, byte[][] bild2) {
        int zeilen = bild1.length;
        if(bild1.length < bild2.length) {
            zeilen = bild2.length;
        }
        int spalten = bild1[0].length;
        if(spalten < bild2[0].length) {
            spalten = bild2[0].length;
        }
        byte[][] res = new byte[zeilen][spalten];
        for (int i = 0; i < zeilen; i++) {
            for (int j = 0; j < spalten; j++) {
                if(i < bild1.length && j < bild1[0].length && i < bild2.length && j < bild2[0].length) {
                    res[i][j] = (byte) ((bild1[i][j] + bild2[i][j]) / 2);
                } else {
                    if(i < bild1.length && j < bild1[0].length) {
                        res[i][j] = bild1[i][j];
                    } else if (i < bild2.length && j < bild2[0].length) {
                        res[i][j] = bild2[i][j];
                    } else {
                        res[i][j] = 0;
                    }
                }
            }
        }
        return res;
    }
    public static void ausgabe(byte[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                System.out.print(a[i][j]);
            }
            System.out.println();
        }
    }
}
