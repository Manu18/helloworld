import java.util.*;

public class Ulam{
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int a = s.nextInt();
		System.out.println(ulam(a));
	
	}
	public static int ulam(int n) {
		if (n == 1) {
			return 1;
		}
		if (n % 2 == 0) {
			System.out.println(n / 2);
			return ulam(n / 2);
		} else {
			System.out.println(3*n + 1);
			return ulam(3*n + 1);
		}
	}
}