import java.util.Arrays;

public class Sieb {
    public static void main(String[] args) {
        int n = 12;
        int[] p = sieb(n);
        System.out.println(Arrays.toString(p));
    }
    public static int[] sieb(int n) {
        boolean[] s = new boolean[n-1];
        for (int i = 2; i < s.length+2; i++) {
            for (int j = i+1; j < s.length+2; j++) {
                if (j % i == 0) {
                    s[j-2] = true;
                }
            }
        }
        int[] p = new int[0];
        for (int i = 0; i < s.length; i++) {
            if(!s[i]) {
                int[] f = new int[p.length + 1];
                for (int j = 0; j < p.length; j++) {
                    f[j] = p[j];
                }
                f[p.length] = i + 2;
                p = f;
            }
        }
        return p;
    }
}
