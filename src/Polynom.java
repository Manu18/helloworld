public class Polynom {
    double[] koeffizienten;
    static int anzahl = 0;

    public Polynom(double[] koeffizienten) {
        int k = 0;
        int start = 0;
        for (int i = 0; i < koeffizienten.length; i++) {
            if (koeffizienten[i] != 0) {
                start = i;
                k = koeffizienten.length - i;
                break;
            }
        }
        double[] polynom = new double[k];
        for (int i = 0; i < polynom.length; i++) {
            polynom[i] = koeffizienten[i+start];
        }

        ///
        int ende = koeffizienten.length;
        for (int i = polynom.length; i > 0; i--) {
            if (polynom[i-1] != 0) {
                ende = i;
                break;
            }
        }

        double[] polynom2 = new double[ende];
        for (int i = 0; i < ende; i++) {
            polynom2[i] = polynom[i];
        }

        anzahl++;
        this.koeffizienten = polynom2;
    }

    public String toString() {
        String ausgabe = "";
        if (this.koeffizienten.length > 1) {
            ausgabe = koeffizienten[0] + "*x^" + 0;
            for (int i = 1; i < this.koeffizienten.length; i++) {
                if(koeffizienten[i] != 0) {
                    ausgabe = koeffizienten[i] + "*x^" + i + " + " + ausgabe;
                }
            }
        } else {
            ausgabe = koeffizienten[0] + "*x^" + 0;
        }
        return ausgabe;
    }

    public double auswerten(double x) {
        double ausgabe = 0;
        for (int i = 0; i < this.koeffizienten.length; i++) {
            ausgabe += this.koeffizienten[i] * Math.pow(x, i);

        }
        return ausgabe;
    }

    public static int getAnzahl() {
        return anzahl;
    }

    public Polynom PolynomAddieren(Polynom q) {
        int länge = 0;
        if (this.koeffizienten.length >= q.koeffizienten.length) {
            länge = this.koeffizienten.length;
        } else {
            länge = q.koeffizienten.length;
        }
        double[] r = new double[länge];
        for (int i = 0; i < r.length; i++) {
            if ((i <= this.koeffizienten.length) && (i <= q.koeffizienten.length)) {
                r[i] = this.koeffizienten[i] + q.koeffizienten[i];
            } else if (i > this.koeffizienten.length) {
                r[i] = q.koeffizienten[i];
            } else if (i > q.koeffizienten.length) {
                r[i] = this.koeffizienten[i];
            }
        }
        return new Polynom(r);
    }

    public Polynom ableiten() {
        // [3,2,1] -- 1x2 2x 3...
        double[] r = new double[this.koeffizienten.length-1];
        for (int i = 0; i < r.length; i++) {
            r[i] = koeffizienten[i+1];
            if(i > 0) {
                r[i] *= (i+1);
            }
        }
        return new Polynom(r);
    }

}
