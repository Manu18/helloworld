public class Verschluesselung {
	private int pubKey;
	private int privKey;
	private int p;
	private int q;
	private int e;
	private int n;
	
	public Verschluesselung() {
		this.q = Primzahl.generierePrime();
		this.p = Primzahl.generierePrime();
		this.n = q * p;
		this.e = (int) (Math.random()) * 100;
		int phi = (p - 1) * (q - 1);
		//inverse
		int arr[] = euklid(e,phi);
		int d = arr[0];
		int k = arr[1];
		if (((e*d) + (k*phi)) == 1) {
			privKey = d;
			System.out.println("Erfolgreich Key generiert");
		} else {
			System.out.println("Fehler Key nicht generiert");
			throw new RuntimeException();
		}
	}

	public String verschluesseln(String s) {
		
		return "";
	}
	
	public String entschluesseln() {
	
		return "";
	}
	
	public int[] euklid(int a, int b) {
		int g, u, v, q, r, s, t;
		u = t = 1;
		v = s = 0;
	
		while(b > 0) {
			q = a / b;
			r = a - q * b; 
			a = b;
			b = r;
			r = u - q * s; 
			u = s;
			s = r;
			r = v - q * t;
			v = t; 
			t = r;
		}
		g = a;
		int[] arr = new int[2];
		arr[0] = u;
		arr[1] = v;
		return arr;
	}

}