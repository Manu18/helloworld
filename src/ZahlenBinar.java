import java.util.*;

public class ZahlenBinar {	
	public static void main(String[] args) {
		// anzahl der bits ab 1
		int bits = Integer.parseInt(args[0]);
		// anzahl des bits ab 0
		int k = Integer.parseInt(args[1]);
		System.out.println(Arrays.toString(binar(bits,k)));
	}
	
	public static int[] binar(int bits, int k) {
		int[] a = new int[1];
		int zaehler = 0;
		String s = "";
		for(int i = 0; i < k; i++) {
			s += "0";
		}
		s = "1" + s;
		int b = Integer.parseInt(s,2);
		System.out.println(s + " " + b);
		for (int i = 0; i < Math.pow(2,bits); i++)  {
			if ((i & b) == b) {
				System.out.println(i);
				String binary = Integer.toBinaryString(i);
				System.out.println(binary);
				int[] bin = new int[zaehler + 1];
				for (int j = 0; j < a.length; j++) {
					bin[j] = a[j];
				}
				bin[zaehler] = i;
				a = bin;
				zaehler++;
			}
		}
		return a;		
	}
}