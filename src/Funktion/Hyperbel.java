package Funktion;

public class Hyperbel extends Function {
    double a;
    Hyperbel(double a) {
        this.a = a;
    }
    double map(double x) {
        return a / x;
    }
}
