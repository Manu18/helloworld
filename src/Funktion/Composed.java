package Funktion;

public class Composed extends Function {
    Function f;
    Function g;
    Composed(Function f, Function g) {
        this.f = f;
        this.g = g;
    }
    double map(double x) {
        return g.map(f.map(x));
    }
}
