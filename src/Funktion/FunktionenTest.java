package Funktion;

public class FunktionenTest {
    public static void main(String[] args) {
        Parabel a = new Parabel(1d,-2d,2d);
        a.print(-5,5,0.1);

        Function f = new Parabel(1.0, -2.0, 2);
        Function g = new Hyperbel(1.0);
        Function h = new Composed(f, g);
        h.print(-1.0, 1.0, 0.1);
    }
}
