package Funktion;

public class Function {
    double map(double x) {
        return x;
    }
    void print(double klein, double groß, double abstand) {
        String s = "";
        for(double i = klein; i <= groß; i = i + abstand) {
            s += map(i) + " ";
        }
        System.out.println(s);
    }
}
