package Funktion;

public class Parabel extends Function {
    double a; //x2
    double b; //x1
    double c; //x0
    Parabel(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    double map(double x) {
        return (a*x*x) + (b*x) + c;
    }
}
