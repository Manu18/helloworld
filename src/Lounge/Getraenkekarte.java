package Lounge;

public class Getraenkekarte {
    public static void main(String[] args) {
        Getraenkekarte gk = new Getraenkekarte();
        gk.neuesGetraenk(new Getraenk("Cola", 200, 250, 0));
        gk.neuesGetraenk(new Getraenk(" Bier ", 200, 200, 16));
        gk.neuesGetraenk(new Getraenk("Vodka", 20, 300, 18));
        System.out.println(gk);
        Getraenk g = gk.suchen("Cola");
        if(g != null) {
            System.out.println(g);
        }
    }
    Getraenk[] getraenke = new Getraenk[0];

    public Getraenkekarte() {

    }

    public void neuesGetraenk(Getraenk g) {
        Getraenk[] neueKarte = new Getraenk[getraenke.length + 1];
        for(int i = 0; i < getraenke.length; i++) {
            neueKarte[i] = getraenke[i];
        }
        neueKarte[getraenke.length] = g;
        getraenke = neueKarte ;
    }

    public Getraenk suchen(String name) {
        for (int i = 0; i < getraenke.length; i++) {
            if(getraenke[i].getName() == name) {
                return getraenke[i];
            }
        }
        return null;
    }

    public String toString() {
        String s = "";
        for(int i = 0; i < getraenke.length; i++) {
            s += getraenke[i] + "\n";
        }
        return s;
    }

}
