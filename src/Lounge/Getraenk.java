package Lounge;

public class Getraenk {
    public static void main(String[] args) {
        Getraenk cola = new Getraenk("Cola", 200, 250, 0);
        Getraenk bier = new Getraenk("Bier", 200, 200, 16);
        Getraenk vodka = new Getraenk ("Vodka" , 20, 300, 18);

        System.out.println(cola);
        System.out.println(bier);
        System.out.println(vodka);
    }
    String name;
    int menge;
    int preis;
    int altersfreigabe;

    public Getraenk(String name, int menge, int preis, int altersfreigabe) {
        this.name = name;
        this.menge = menge;
        this.preis = preis;
        this.altersfreigabe = altersfreigabe;
    }
    public String toString() {
        String erg = "Name: " + name + ", Menge: " + menge + ", Preis: " + preis +", Altersfreigabe: " + altersfreigabe;
        return erg;
    }

    public String getName() {
        return this.name;
    }
}
