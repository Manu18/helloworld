package Lounge;

public class Person_alt {
    public static void main(String[] args) {
        Person_alt paul = new Person_alt("Paul", 16);
        System.out.println(paul);
    }
    String name;
    int alter;

    public String toString() {
        String erg = name + " (" + alter + ")";
        return erg;
    }

    public Person_alt(String name, int alter) {
        this.name = name;
        this.alter = alter;
    }
}
