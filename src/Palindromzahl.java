import java.util.Scanner;

public class Palindromzahl {
    public static int länge(int input) {
        int i = 0;
        if (input >= 0) {
            i = 1;
            if(input >= 10) {
                i = 2;
                if(input >= 100) {
                    i = 3;
                    if(input >= 1000) {
                        i = 4;
                        if(input >= 10000) {
                            i = 5;
                            if(input >= 100000) {
                                i = 6;
                                if(input >= 1000000) {
                                    i = 7;
                                    if(input >= 10000000) {
                                        i = 8;
                                        if(input >= 100000000) {
                                            i = 9;
                                            if(input >= 1000000000) {
                                            i = 10;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return i;
    }


    public static int spiegeln(int zahl) {
        int umgekehrteZahl = 0;
        int zahlOriginal = zahl;
        for (int i = 0; i < länge(zahlOriginal); i++) {
            umgekehrteZahl *= 10;
            umgekehrteZahl += (zahl % 10);
            zahl /= 10;
        }
        return umgekehrteZahl;
    }

   public static int spiegelAddieren(int zahl) {
        int spiegelZahl = spiegeln(zahl);
        int spiegelAdd = spiegelZahl + zahl;
        return spiegelAdd;
    }

    public static boolean palindromTest(int zahl) {
        boolean polinTest = spiegeln(zahl) == zahl;
        return polinTest;
    }

    public static void main(String[] args) {
        int n = Integer.valueOf(args[0]);
        System.out.println(spiegeln(n));
        System.out.println(spiegelAddieren(n));

        int k = n;
        while (!palindromTest(k)) {
            k = spiegelAddieren(k);
        }
        System.out.println(k);
    }
}
