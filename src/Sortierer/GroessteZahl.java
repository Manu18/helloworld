import java.util.*;
public class GroessteZahl {
	public static int counter = 0;
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int anzahl = s.nextInt();
        int[] a = new int[anzahl];
        int zähler = 0;
        while (s.hasNextInt()) {
            a[zähler] = s.nextInt();
            zähler++;
        }
        int[] sortiert = sortieren(a);
        System.out.println(Arrays.toString(sortiert));
		System.out.println(counter);
        s.close();
    }

    public static int[] sortieren(int[] a) {
        int richtig = 1;
        for (int i = 0; i < a.length - 1; i++) {
            if(a[i] <= a[i+1]) {
                richtig++;
            } else {
                int k = a[i];
                int l = a[i+1];
                a[i] = l;
                a[i+1] = k;
            }
			counter++;
            System.out.println(Arrays.toString(a));
        }
        if(richtig != a.length) {
            a = sortieren(a);
        }
        return a;
    }
}