import java.util.*;
public class SortierenAuswaehlen {
	public static int counter = 0;
	public static int[] sortierenDurchAuswaehlen(int[] a) {
		int tmp, min; 
		for(int i = 0; i < a.length - 1; i++) {
			min = i;
			for(int j = i+1; j < a.length; j++) {
				counter++;
				if(a[j] < a[min]) 
					min = j;
				System.out.println(Arrays.toString(a));
			}
			tmp = a[i];
			a[i] = a[min];
			a[min] = tmp;
		}
		return a;
	}
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
        int anzahl = s.nextInt();
        int[] a = new int[anzahl];
        int zähler = 0;
        while (s.hasNextInt()) {
            a[zähler] = s.nextInt();
            zähler++;
        }
        int[] sortiert = sortierenDurchAuswaehlen(a);
        System.out.println(Arrays.toString(sortiert));
		System.out.println(counter);
        s.close();
	}
}	