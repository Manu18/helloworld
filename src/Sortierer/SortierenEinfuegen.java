import java.util.*;
public class SortierenEinfuegen {
	public static int counter;
	public static int[] sortierenDurchEinfuegen(int[] a) {
		for(int i = 1; i < a.length; i++) {
			int wert = a[i];
			int j = i - 1;
			while((j >= 0) && (a[j] > wert)) {
				a[j+1] = a[j];
				j--; 
				counter++;
				System.out.println(Arrays.toString(a));
			}
			a[j+1] = wert; 
		} 
		return a;
	}
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int anzahl = s.nextInt();
        int[] a = new int[anzahl];
        int zähler = 0;
        while (s.hasNextInt()) {
            a[zähler] = s.nextInt();
            zähler++;
        }
        int[] sortiert = sortierenDurchEinfuegen(a);
        System.out.println(Arrays.toString(sortiert));
		System.out.println(counter);
        s.close();
    }
 }