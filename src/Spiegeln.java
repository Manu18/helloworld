public class Spiegeln {
    public static void main(String[] args) {
        String s = "hallo";
        System.out.println(s + " gespiegelt ist " + spiegeln(s));
    }

    public static String spiegeln(String s) {
        String gespiegelt = "";
        for (int i = (s.length() - 1); i >= 0; i--) {
            gespiegelt += s.charAt(i);
        }
        return gespiegelt;
    }
}
