public class Hanoi {
    public static int schritt = 64;
    public static void main(String[] args) {
        int n = 4;
        // bewege Stapel mit n=3 Scheiben von Stab 0 nach Stab 1 über Stab 2
        Hanoi(0,1,2,n);
    }

    static void Hanoi(int Quelle, int Senke, int Arbeitsbereich, int n) {
        if (n == 1) bewege(Quelle, Senke);
        else {
            Hanoi(Quelle, Arbeitsbereich, Senke, n - 1);
            bewege(Quelle, Senke);
            Hanoi(Arbeitsbereich, Senke, Quelle, n - 1);
        }
    }

    static void bewege(int Quelle, int Senke) {
        System.out.println("Bewege Scheibe von " + Quelle + " nach " + Senke);
        System.out.println(schritt);
        schritt++;
    }
}