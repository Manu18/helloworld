public class PolynomTest {
    public static void main(String[] args) {
        //P(x) = 1·x2 +2·x+3
        double[] koeffizienten = {0d,0d,1d};
        Polynom p1 = new Polynom(koeffizienten);

        System.out.println(p1);
        System.out.println(p1.auswerten(2));
        System.out.println(Polynom.getAnzahl());

        double[] koeffizienten2 = {7d,5d,1d,2d};
        Polynom p2 = new Polynom(koeffizienten2);
        System.out.println(p2);
        System.out.println(p2.ableiten());

        double[] koeffizienten3 = {4d,3d,2d,1d};
        Polynom p3 = new Polynom(koeffizienten2);
        System.out.println(p3);
        Polynom r = p3.PolynomAddieren(p2);
        System.out.println(r);
        Polynom r1 = r.ableiten();
        System.out.println(r1);
        Polynom r2 = r1.ableiten();
        System.out.println(r2);
        System.out.println(r.auswerten(477));
        System.out.println(r1.auswerten(477));
        System.out.println(r2.auswerten(477));
    }
}
