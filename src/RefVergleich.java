import java.util.Arrays;

public class RefVergleich {
    public static void main(String[] args) {
        String[] feld = {"Hello", "world"};
        // Erstellt ein Feld mit 2 Strings
        System.out.println(feld);
        System.out.println(referenz(feld));
        System.out.println(kopieren(feld));

        vergleichRef(kopieren(feld), referenz(feld));
        /*

        [Ljava.lang.String;@6acbcfc0
        [Ljava.lang.String;@5f184fc6
        [Ljava.lang.String;@5b6f7412
        Referenz: false
        Referenz: false
        Inhalt true
        Inhalt true

         */
    }
    public static String[] referenz(String[] feld) {
        String[] resultat = new String[2 * feld.length];
        for (int i = 0; i < feld.length; i++) {
            resultat[2*i] = feld[i];
            resultat[2*i+1] = feld[i];
        }

        return resultat;
    }
    public static String[] kopieren(String[] feld) {
        String[] kopie = new String[2 * feld.length];
        for (int i = 0; i < feld.length; i++) {
            kopie[2*i] = "" + feld[i];
            kopie[2*i+1] = "" + feld[i];
        }

        return kopie;
    }
    public static void vergleichRef(String[] a, String[] b) {
        for (int i = 0; i < (a.length / 2); i++) {
            boolean gleich = a[2 * i] == b[2 * i + 1];
            System.out.println("Referenz: " + gleich);
        }
        for (int i = 0; i < (a.length / 2); i++) {
            boolean gleich = String.valueOf(a[2 * i]).equals(String.valueOf(b[2 * i + 1]));
            System.out.println("Inhalt " + gleich);
        }
    }

}
