package Flugzeug;

public class Passagierflugzeug extends Flugzeug {
    double schub;
    public static int passagierflugzeuge = 0;

    public Passagierflugzeug(double spannweite, int sitzplaetze, double schub) {
        super(spannweite, sitzplaetze);
        this.schub = schub;
        passagierflugzeuge++;
    }

    public String toString() {
        String ausgabe = "";
        ausgabe = "Spannweite: " + this.spannweite + ", Sitze: " + this.sitzplaetze + ", Schub: " + this.schub + " (" + this.getClass() + ")";
        return ausgabe;
    }

}
