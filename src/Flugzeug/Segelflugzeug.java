package Flugzeug;

public class Segelflugzeug extends Flugzeug {
    public static int segelflugzeuge = 0;

    public Segelflugzeug(double spannweite) {
        super(spannweite, 1);
        segelflugzeuge++;
    }

    public String toString() {
        String ausgabe = "";
        ausgabe = "Spannweite: " + this.spannweite + ", Sitze: " + this.sitzplaetze + " (" + this.getClass() + ")";
        return ausgabe;
    }

}
