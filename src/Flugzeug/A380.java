package Flugzeug;

public class A380 extends Passagierflugzeug{
    public static int a380 = 0;

    public A380() {
        super(79.8,558,1280);
        a380++;
    }

    public String toString() {
        String ausgabe = "";
       // Spannweite : 79.8 , Sitze : 558 , Schub : 1280.0 ( Passagierflugzeug ) A380
        ausgabe = "Spannweite: " + this.spannweite + ", Sitze: " + this.sitzplaetze + ", Schub: " + this.schub + " (Passagierflugzeug) " + this.getClass();
        return ausgabe;
    }

}
