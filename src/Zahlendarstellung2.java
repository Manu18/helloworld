import java.util.Arrays;

public class Zahlendarstellung2 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(ermittleZiffern(0, 2)));
    }

    public static int[] ermittleZiffern(int x, int b) {
        int[] a = new int[0];
        if (x== 0) {
            return new int[] {0};
        }
        while (x != 0) {
            int[] k = new int[a.length + 1];
            for(int i= 0; i < a.length; i++) {
                k[i] = a[i];
            }
            k[k.length - 1] = (x % b);
            x = x / b;
            a = k;
        }
        return a;
    }
}
