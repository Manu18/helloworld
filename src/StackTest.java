public class StackTest {
    public static void main(String[] args) {
        Stack st = new Stack(10);
        System.out.println(st.isEmpty());
        st.push("a");
        st.push("b");
        st.push("c");
        st.push("c");
        System.out.println(st.pop());
        System.out.println(st.isEmpty());
        System.out.println(st.pop());
        System.out.println(st.pop());
        System.out.println(st.pop());
        System.out.println(st.isEmpty());
    }
}
